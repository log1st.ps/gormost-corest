export const INPUT_TYPE_TEXT = 'text';
export const INPUT_TYPE_NUMBER = 'number';
export const INPUT_TYPE_TEXTAREA = 'textarea';

export type availableFormInputTypes =
    typeof INPUT_TYPE_TEXT
    | typeof INPUT_TYPE_NUMBER
    | typeof INPUT_TYPE_TEXTAREA;

export const availableFormInputTypesList = [
    INPUT_TYPE_TEXT,
    INPUT_TYPE_NUMBER,
    INPUT_TYPE_TEXTAREA,
];
