export const CHECKBOX_TYPE_CHECKBOX = 'checkbox';
export const CHECKBOX_TYPE_RADIO = 'radio';
export const CHECKBOX_TYPE_SWITCH = 'switch';

export type availableFormCheckboxTypes =
    typeof CHECKBOX_TYPE_CHECKBOX
    | typeof CHECKBOX_TYPE_RADIO
    | typeof CHECKBOX_TYPE_SWITCH;

export const availableFormCheckboxTypesList = [
    CHECKBOX_TYPE_CHECKBOX,
    CHECKBOX_TYPE_RADIO,
    CHECKBOX_TYPE_SWITCH,
];
