export const TAB_TYPE_DEFAULT = 'default';
export const TAB_TYPE_STEP = 'step';

export type availableTabTypes =
    typeof TAB_TYPE_DEFAULT
    | typeof TAB_TYPE_STEP;

export const availableTabTypesList = [
    TAB_TYPE_DEFAULT,
    TAB_TYPE_STEP,
];
