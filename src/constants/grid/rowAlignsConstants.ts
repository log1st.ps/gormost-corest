export const ROW_ALIGN_START = 'start';
export const ROW_ALIGN_CENTER = 'center';
export const ROW_ALIGN_END = 'end';

export type availableRowAligns =
    typeof ROW_ALIGN_START
    | typeof ROW_ALIGN_CENTER
    | typeof ROW_ALIGN_END;

export const availableRowAlignsList = [
    ROW_ALIGN_START,
    ROW_ALIGN_CENTER,
    ROW_ALIGN_END,
];
