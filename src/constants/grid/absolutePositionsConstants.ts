export const ABSOLUTE_POSITION_TOP = 'top';
export const ABSOLUTE_POSITION_CENTER = 'center';
export const ABSOLUTE_POSITION_BOTTOM = 'bottom';

export const ABSOLUTE_ALIGN_LEFT = 'top';
export const ABSOLUTE_ALIGN_CENTER = 'center';
export const ABSOLUTE_ALIGN_RIGHT = 'end';

export type availableAbsolutePositions =
    typeof ABSOLUTE_POSITION_TOP
    | typeof ABSOLUTE_POSITION_CENTER
    | typeof ABSOLUTE_POSITION_BOTTOM;

export const availableAbsolutePositionsList = [
    ABSOLUTE_POSITION_TOP,
    ABSOLUTE_POSITION_CENTER,
    ABSOLUTE_POSITION_BOTTOM,
];

export type availableAbsoluteAligns =
    typeof ABSOLUTE_ALIGN_LEFT
    | typeof ABSOLUTE_ALIGN_CENTER
    | typeof ABSOLUTE_ALIGN_RIGHT;

export const availableAbsoluteAlignsList = [
    ABSOLUTE_ALIGN_LEFT,
    ABSOLUTE_ALIGN_CENTER,
    ABSOLUTE_ALIGN_RIGHT,
];

export type dim = number | null;
