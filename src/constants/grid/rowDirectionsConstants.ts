export const ROW_DIRECTION_COLUMN = 'column';
export const ROW_DIRECTION_ROW = 'row';
export const ROW_DIRECTION_COLUMN_REVERSE = 'columnReverse';
export const ROW_DIRECTION_ROW_REVERSE = 'rowReverse';

export type availableRowDirections =
    typeof ROW_DIRECTION_COLUMN
    | typeof ROW_DIRECTION_COLUMN_REVERSE
    | typeof ROW_DIRECTION_ROW
    | typeof ROW_DIRECTION_ROW_REVERSE;

export const availableRowDirectionsList = [
    ROW_DIRECTION_COLUMN,
    ROW_DIRECTION_COLUMN_REVERSE,
    ROW_DIRECTION_ROW,
    ROW_DIRECTION_ROW_REVERSE,
];
