export const ROW_JUSTIFY_START = 'start';
export const ROW_JUSTIFY_CENTER = 'center';
export const ROW_JUSTIFY_END = 'end';
export const ROW_JUSTIFY_BETWEEN = 'between';
export const ROW_JUSTIFY_AROUND = 'around';

export type availableRowJustifies =
    typeof ROW_JUSTIFY_START
    | typeof ROW_JUSTIFY_CENTER
    | typeof ROW_JUSTIFY_END
    | typeof ROW_JUSTIFY_BETWEEN
    | typeof ROW_JUSTIFY_AROUND;

export const availableRowJustifiesList = [
    ROW_JUSTIFY_START,
    ROW_JUSTIFY_CENTER,
    ROW_JUSTIFY_END,
    ROW_JUSTIFY_BETWEEN,
    ROW_JUSTIFY_AROUND,
];
