export const BUTTON_SIZE_XS = 'xs';
export const BUTTON_SIZE_SM = 'sm';
export const BUTTON_SIZE_MD = 'md';
export const BUTTON_SIZE_LG = 'lg';
export const BUTTON_SIZE_XL = 'xl';
export const BUTTON_SIZE_XLG = 'xlg';

export type availableButtonSizes =
    typeof BUTTON_SIZE_XS
    | typeof BUTTON_SIZE_SM
    | typeof BUTTON_SIZE_MD
    | typeof BUTTON_SIZE_LG
    | typeof BUTTON_SIZE_XL
    | typeof BUTTON_SIZE_XLG;

export const availableButtonSizesList = [
    BUTTON_SIZE_XS,
    BUTTON_SIZE_MD,
    BUTTON_SIZE_LG,
    BUTTON_SIZE_XL,
    BUTTON_SIZE_XLG,
];
