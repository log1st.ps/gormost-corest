export const BUTTON_STATE_PURE = 'pure';
export const BUTTON_STATE_PRIMARY = 'primary';
export const BUTTON_STATE_SECONDARY = 'secondary';
export const BUTTON_STATE_TERTIARY = 'tertiary';
export const BUTTON_STATE_QUATERNARY = 'quaternary';
export const BUTTON_STATE_QUINARY = 'quinary';
export const BUTTON_STATE_FONT_REGULAR = 'fontRegular';
export const BUTTON_STATE_FONT_LIGHT = 'fontLight';
export const BUTTON_STATE_FONT_BOLD = 'fontBold';

type statesType =
    typeof BUTTON_STATE_PURE
    | typeof BUTTON_STATE_PRIMARY
    | typeof BUTTON_STATE_SECONDARY
    | typeof BUTTON_STATE_TERTIARY
    | typeof BUTTON_STATE_QUATERNARY
    | typeof BUTTON_STATE_FONT_LIGHT
    | typeof BUTTON_STATE_FONT_REGULAR
    | typeof BUTTON_STATE_FONT_BOLD
    | typeof BUTTON_STATE_QUINARY
    ;

export type availableButtonStates =
    statesType
    | statesType[];

export const availableButtonStatesList = [
    BUTTON_STATE_PURE,
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_SECONDARY,
    BUTTON_STATE_TERTIARY,
    BUTTON_STATE_QUATERNARY,
    BUTTON_STATE_FONT_LIGHT,
    BUTTON_STATE_FONT_REGULAR,
    BUTTON_STATE_FONT_BOLD,
    BUTTON_STATE_QUINARY,
];
