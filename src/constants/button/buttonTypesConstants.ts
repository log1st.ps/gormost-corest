export const BUTTON_TYPE_BUTTON = 'button';
export const BUTTON_TYPE_LINK = 'link';

export type availableButtonTypes =
    typeof BUTTON_TYPE_LINK
    | typeof BUTTON_TYPE_BUTTON;

export const availableButtonTypesList = [
    BUTTON_TYPE_LINK,
    BUTTON_TYPE_BUTTON,
];
