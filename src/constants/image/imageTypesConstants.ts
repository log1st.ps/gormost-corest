export const IMAGE_TYPE_BLOCK = 'div';
export const IMAGE_TYPE_IMG = 'img';

export type availableImageTypes =
    typeof IMAGE_TYPE_BLOCK
    | typeof IMAGE_TYPE_IMG;

export const availableImageTypesList = [
    IMAGE_TYPE_BLOCK,
    IMAGE_TYPE_IMG,
];
