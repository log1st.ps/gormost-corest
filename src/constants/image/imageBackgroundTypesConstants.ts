export const IMAGE_BG_TYPE_CONTAIN  = 'contain';
export const IMAGE_BG_TYPE_COVER  = 'cover';
export const IMAGE_BG_TYPE_AUTO = 'auto';

export type availableBgTypes =
    typeof IMAGE_BG_TYPE_CONTAIN
    | typeof IMAGE_BG_TYPE_COVER;

export const availableBgTypesList = [
    IMAGE_BG_TYPE_AUTO,
    IMAGE_BG_TYPE_CONTAIN,
    IMAGE_BG_TYPE_COVER,
];
