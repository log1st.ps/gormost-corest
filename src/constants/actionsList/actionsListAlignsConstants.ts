export const ACTIONS_LIST_ALIGN_START = 'start';
export const ACTIONS_LIST_ALIGN_CENTER = 'center';
export const ACTIONS_LIST_ALIGN_END = 'end';
export const ACTIONS_LIST_ALIGN_BETWEEN = 'between';
export const ACTIONS_LIST_ALIGN_AROUND = 'around';
export const ACTIONS_LIST_ALIGN_EVENLY = 'evenly';

export type availableActionsListAligns =
    typeof ACTIONS_LIST_ALIGN_START
    | typeof ACTIONS_LIST_ALIGN_CENTER
    | typeof ACTIONS_LIST_ALIGN_END
    | typeof ACTIONS_LIST_ALIGN_BETWEEN
    | typeof ACTIONS_LIST_ALIGN_AROUND
    | typeof ACTIONS_LIST_ALIGN_EVENLY;

export const availableActionsListAlignsList = [
    ACTIONS_LIST_ALIGN_START,
    ACTIONS_LIST_ALIGN_CENTER,
    ACTIONS_LIST_ALIGN_END,
    ACTIONS_LIST_ALIGN_BETWEEN,
    ACTIONS_LIST_ALIGN_AROUND,
    ACTIONS_LIST_ALIGN_EVENLY,
];
