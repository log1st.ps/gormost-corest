export const ICON_TYPE_PLUS = 'plus';

export type availableIconTypes =
    typeof ICON_TYPE_PLUS;

export const availableIconTypesList = [
    ICON_TYPE_PLUS,
];
