export const COLOR_PRIMARY = 'primary';
export const COLOR_PRIMARY_LIGHTER = 'primaryLighter';
export const COLOR_PRIMARY_DARKER = 'primaryDarker';
export const COLOR_SECONDARY = 'secondary';
export const COLOR_TERTIARY = 'tertiary';
export const COLOR_QUATERNARY = 'quaternary';
export const COLOR_QUINARY = 'quinary';
export const COLOR_SENARY = 'senary';
export const COLOR_SEPTENARY = 'septenary';
export const COLOR_SEPTENARY_LIGHTER = 'septenaryLighter';
export const COLOR_WARNING = 'warning';
export const COLOR_SUCCESS = 'success';
export const COLOR_SUCCESS_DARKER = 'successDarker';

export type availableStylerColors =
    typeof COLOR_PRIMARY
    | typeof COLOR_PRIMARY_LIGHTER
    | typeof COLOR_PRIMARY_DARKER
    | typeof COLOR_SECONDARY
    | typeof COLOR_TERTIARY
    | typeof COLOR_QUATERNARY
    | typeof COLOR_QUINARY
    | typeof COLOR_SENARY
    | typeof COLOR_SEPTENARY
    | typeof COLOR_SEPTENARY_LIGHTER
    | typeof COLOR_WARNING
    | typeof COLOR_SUCCESS
    | typeof COLOR_SUCCESS_DARKER;


export const availableStylerColorsList = [
    COLOR_PRIMARY,
    COLOR_PRIMARY_LIGHTER,
    COLOR_PRIMARY_DARKER,
    COLOR_SECONDARY,
    COLOR_TERTIARY,
    COLOR_QUATERNARY,
    COLOR_QUINARY,
    COLOR_SENARY,
    COLOR_SEPTENARY,
    COLOR_SEPTENARY_LIGHTER,
    COLOR_WARNING,
    COLOR_SUCCESS,
    COLOR_SUCCESS_DARKER,
];
