import { VNode } from 'vue';
import { Component } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';

@Component
class EmptyRender extends VueComponent<{}> {

    public render(): VNode {
        return (
            <div/>
        );
    }
}

export default EmptyRender;
