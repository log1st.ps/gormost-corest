import {Component, Emit, Mixins, Prop} from 'vue-property-decorator';
import {Route} from 'vue-router';
import {VueComponent} from 'vue-tsx-helper';
import {
    availableButtonSizes,
    availableButtonSizesList,
    BUTTON_SIZE_SM,
} from '../../../constants/button/buttonSizesConstants';
import {
    availableButtonStates,
    availableButtonStatesList,
    BUTTON_STATE_PURE,
} from '../../../constants/button/buttonStatesConstants';
import {
    availableButtonTypes,
    availableButtonTypesList,
    BUTTON_TYPE_BUTTON,
    BUTTON_TYPE_LINK,
} from '../../../constants/button/buttonTypesConstants';
import {availableIconTypes, availableIconTypesList} from '../../../constants/icon/iconTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIEvent from '../../../helpers/documentation/APIEvent';
import APIProp, {
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_NUMBER,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import {log, LOG_CATEGORY_COMPONENT_METHOD} from '../../../helpers/Logger';
import EmptyRender from '../../../mixins/EmptyRender';
import {IButton} from './ButtonElementMixin.d';

@Component
class Mixin extends VueComponent<IButton> {
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        { name: 'default', description: 'Primary text block' },
        { name: 'before', description: 'Block to the left side of default' },
        { name: 'after', description: 'Block to the right side of default' },
    ],
})
@Component
class ButtonElementMixin
    extends Mixins(Mixin, EmptyRender)
    implements IButton {

    get computedRoute(): Route | string | undefined {
        return this.type === BUTTON_TYPE_LINK ? (this.url || '') : undefined;
    }

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Text to be placed in default slot',
    })
    @Prop()
    public text ?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Size',
        defaultValue: BUTTON_SIZE_SM,
        availableValues: availableButtonSizesList,
    })
    @Prop()
    public size ?: availableButtonSizes;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Type of button',
        availableValues: availableButtonTypesList,
    })
    @Prop({ default: () => BUTTON_TYPE_BUTTON })
    public type ?: availableButtonTypes;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'State of button; might be multiple',
        availableValues: availableButtonStatesList,
    })
    @Prop({ default: () => BUTTON_STATE_PURE })
    public state ?: availableButtonStates;

    @APIProp({
        type: [PROP_TYPE_OBJECT, PROP_TYPE_STRING],
        description:
            `Route object or string in case of button type equals to *${BUTTON_TYPE_LINK}*`,
    })
    @Prop()
    public url ?: Route;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Describes component as disabled',
        defaultValue: false,
    })
    @Prop({ default: false })
    public isDisabled ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Describes component as block',
        defaultValue: false,
    })
    @Prop({ default: false })
    public isBlock ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Describes that component has rounded borders',
        defaultValue: false,
    })
    @Prop({ default: false })
    public isRounded ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Describes component as square',
        defaultValue: false,
    })
    @Prop({ default: false })
    public isSquare ?: boolean;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Icon before default slot',
        availableValues: availableIconTypesList,
        isSugar: true,
        defaultValue: null,
    })
    @Prop({ default: null })
    public leftIcon ?: availableIconTypes;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Icon after default slot',
        availableValues: availableIconTypesList,
        isSugar: true,
        defaultValue: null,
    })
    @Prop({ default: null })
    public rightIcon ?: availableIconTypes;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Describe that button is in loading state',
        isSugar: true,
        defaultValue: false,
    })
    @Prop()
    public isLoading ?: boolean;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Backgrond loading progress',
        isSugar: true,
        defaultValue: null,
    })
    @Prop({ default: () => null })
    public loadingProgress ?: number;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        defaultValue: 'Should button if its type is link use router classes',
    })
    @Prop()
    public withRouterClasses ?: boolean;

    @APIProp({
        type: PROP_TYPE_OBJECT,
        defaultValue: {

        },
    })
    @Prop()
    public routerClasses?: {
        active?: string;
        exactActive?: string;
    };

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Tab index',
    })
    @Prop()
    public tabIndex?: number;

    @APIEvent({
        name: 'click',
    })
    @Emit('click')
    protected onClick() {
        // something
    }

    @APIEvent({
        name: 'doubleClick',
    })
    @Emit('doubleClick')
    protected onDoubleClick() {
        // something
    }
}

export default ButtonElementMixin;
