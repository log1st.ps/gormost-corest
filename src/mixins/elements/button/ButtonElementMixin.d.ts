import { availableButtonStates } from '../../../constants/button/buttonStatesConstants';
import { availableButtonTypes } from '../../../constants/button/buttonTypesConstants';
import { availableIconTypes } from '../../../constants/icon/iconTypesConstants';
import { Route } from 'vue-router';
import { availableButtonSizes } from '../../../constants/button/buttonSizesConstants';
import {VNode} from 'vue';

export interface IButton {
    text?: string;
    size?: availableButtonSizes | string;
    type?: availableButtonTypes;
    state?: availableButtonStates;
    url?: string | Route | {
        name?: string,
        path?: any,
        hash?: any,
        params?: any,
    };
    isDisabled?: boolean;
    isBlock?: boolean;
    isRounded?: boolean;
    isSquare?: boolean;
    leftIcon?: availableIconTypes | (() => VNode) | any;
    rightIcon?: availableIconTypes | (() => VNode) | any;
    isLoading?: boolean;
    loadingProgress?: number;
    withRouterClasses?: boolean;
    tabIndex?: number;
    routerClasses?: {
        active?: string;
        exactActive?: string;
    };
}
