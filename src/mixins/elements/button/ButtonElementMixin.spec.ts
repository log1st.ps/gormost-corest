import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import { BUTTON_STATE_PURE } from '../../../constants/button/buttonStatesConstants';
import { BUTTON_TYPE_BUTTON } from '../../../constants/button/buttonTypesConstants';
import ButtonElementMixin from './ButtonElementMixin';

describe('ButtonElementMixin', () => {
    [
        'text',
        'type',
        'state',
        'url',
        'isDisabled',
        'isBlock',
        'isRounded',
        'isSquare',
        'leftIcon',
        'rightIcon',
        'isLoading',
        'loadingProgress',
    ].forEach((item: string) => {
        const wrapper = shallowMount(ButtonElementMixin);

        it(`contains props.${item}`, () => {
            expect(wrapper.props()).to.contain.keys(item);
        });
    });

    it('emits click event', () => {
        const wrapper = shallowMount(ButtonElementMixin);
        expect(wrapper.vm).to.contains.keys('onClick');
        // @ts-ignore
        wrapper.vm.onClick();
        expect(wrapper.emitted()).to.contains.keys('click');
    });

    it('emits doubleClick event', () => {
        const wrapper = shallowMount(ButtonElementMixin);
        expect(wrapper.vm).to.contains.keys('onDoubleClick');
        // @ts-ignore
        wrapper.vm.onDoubleClick();
        expect(wrapper.emitted()).to.contains.keys('doubleClick');
    });

    it(`has prop.type default value equal to "${BUTTON_TYPE_BUTTON}"`, () => {
        const wrapper = shallowMount(ButtonElementMixin);
        expect(wrapper.vm.type).to.be.equal(BUTTON_TYPE_BUTTON);
    });

    it(`has prop.state default value equal to "${BUTTON_STATE_PURE}"`, () => {
        const wrapper = shallowMount(ButtonElementMixin);
        expect(wrapper.vm.state).to.be.equal(BUTTON_STATE_PURE);
    });
});
