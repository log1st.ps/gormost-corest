import { availableIconTypes } from '../../../constants/icon/iconTypesConstants';

export interface IIcon {
    type: availableIconTypes | string;
    height?: number | string;
    width?: number | string;
}
