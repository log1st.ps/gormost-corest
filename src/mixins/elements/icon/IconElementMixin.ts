import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import { availableIconTypes, availableIconTypesList } from '../../../constants/icon/iconTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_NUMBER, PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import { IIcon } from './IconElementMixin.d';

@Component
class Mixin extends VueComponent<IIcon> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({

})
@Component
class IconElementMixin
    extends Mixins(Mixin, EmptyRender)
    implements IIcon {
    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Icon type',
        availableValues: availableIconTypesList,
    })
    @Prop()
    public type !: availableIconTypes | string;

    @APIProp({
        type: [PROP_TYPE_STRING, PROP_TYPE_NUMBER],
        description: 'Height of icon',
        availableValues: ['Number for px', 'Any css bound format: em, px, %, rem, etc.'],
    })
    @Prop()
    public height ?: number | string;

    @APIProp({
        type: [PROP_TYPE_STRING, PROP_TYPE_NUMBER],
        description: 'Width of icon',
        availableValues: ['Number for px', 'Any css bound format: em, px, %, rem, etc.'],
    })
    @Prop()
    public width ?: number | string;
}

export default IconElementMixin;
