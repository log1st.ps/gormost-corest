import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import IconElementMixin from './IconElementMixin';

describe('IconElementMixin', () => {
    [
        'type',
        'height',
        'width',
    ].forEach((item: string) => {
        it(`contains props.${item}`, () => {
            const wrapper = shallowMount(IconElementMixin);

            expect(wrapper.props()).to.contains.keys(item);
        });
    });
});
