import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import { availableTypographyTypes } from '../../../constants/typography/typographyTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {
    IBaseTypography,
    ICaptionTypography,
    ICommonTypography,
    IHintTypography,
    IPrimaryTypography,
    IQuaternaryTypography,
    IQuinaryTypography,
    ISecondaryTypography,
    ISenaryTypography,
    ISeptenaryTypography,
    ITertiaryTypography,
    ITextTypography,
} from './TypographyComponentMixin.d';

@Component
class CommonMixin extends VueComponent<ICommonTypography> implements ICommonTypography {
    @APIProp({
        type: PROP_TYPE_STRING,
    })
    @Prop()
    public text ?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
    })
    @Prop()
    public color ?: string;
}

// tslint:disable-next-line:max-classes-per-file
@Component
class BaseMixin extends VueComponent<IBaseTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
export class BaseTypographyComponentMixin
    extends Mixins(CommonMixin, BaseMixin)
    implements IBaseTypography {

    public type ?: availableTypographyTypes;
}

// tslint:disable-next-line:max-classes-per-file
@Component
class PrimaryMixin extends VueComponent<IPrimaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SecondaryMixin extends VueComponent<ISecondaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class TertiaryMixin extends VueComponent<ITertiaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class QuaternaryMixin extends VueComponent<IQuaternaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class QuinaryMixin extends VueComponent<IQuinaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SenaryMixin extends VueComponent<ISenaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class SeptenaryMixin extends VueComponent<ISeptenaryTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class TextMixin extends VueComponent<ITextTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class HintMixin extends VueComponent<IHintTypography> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class CaptionMixin extends VueComponent<ICaptionTypography> {}

const componentDocumentation = {
    slots: [
        {
            name: 'default',
            description: 'Instead of content',
        },
    ],
};

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class PrimaryTypographyComponentMixin
    extends Mixins(CommonMixin, PrimaryMixin, EmptyRender) implements IPrimaryTypography {
    public type: 'primary' = 'primary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class SecondaryTypographyComponentMixin
    extends Mixins(CommonMixin, SecondaryMixin, EmptyRender) implements ISecondaryTypography {
    public type: 'secondary' = 'secondary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class TertiaryTypographyComponentMixin
    extends Mixins(CommonMixin, TertiaryMixin, EmptyRender) implements ITertiaryTypography {
    public type: 'tertiary' = 'tertiary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class QuaternaryTypographyComponentMixin
    extends Mixins(CommonMixin, QuaternaryMixin, EmptyRender) implements IQuaternaryTypography {
    public type: 'quaternary' = 'quaternary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class QuinaryTypographyComponentMixin
    extends Mixins(CommonMixin, QuinaryMixin, EmptyRender) implements IQuinaryTypography {
    public type: 'quinary' = 'quinary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class SenaryTypographyComponentMixin
    extends Mixins(CommonMixin, SenaryMixin, EmptyRender) implements ISenaryTypography {
    public type: 'senary' = 'senary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class SeptenaryTypographyComponentMixin
    extends Mixins(CommonMixin, SeptenaryMixin, EmptyRender) implements ISeptenaryTypography {
    public type: 'septenary' = 'septenary';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class TextTypographyComponentMixin
    extends Mixins(CommonMixin, TextMixin, EmptyRender) implements ITextTypography {
    public type: 'text' = 'text';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class HintTypographyComponentMixin
    extends Mixins(CommonMixin, HintMixin, EmptyRender) implements IHintTypography {
    public type: 'hint' = 'hint';
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent(componentDocumentation)
@Component
export class CaptionTypographyComponentMixin
    extends Mixins(CommonMixin, CaptionMixin, EmptyRender) implements ICaptionTypography {
    public type: 'caption' = 'caption';
}
