import { availableTypographyTypes } from '../../../constants/typography/typographyTypesConstants';

export interface ICommonTypography {
    text?: string;
    color?: string;
}

export interface IBaseTypography {
    type?: availableTypographyTypes;
}

export interface IPrimaryTypography extends IBaseTypography {type?: 'primary';}
export interface ISecondaryTypography extends IBaseTypography {type?: 'secondary';}
export interface ITertiaryTypography extends IBaseTypography {type?: 'tertiary';}
export interface IQuaternaryTypography extends IBaseTypography {type?: 'quaternary';}
export interface IQuinaryTypography extends IBaseTypography {type?: 'quinary';}
export interface ISenaryTypography extends IBaseTypography {type?: 'senary';}
export interface ISeptenaryTypography extends IBaseTypography {type?: 'septenary';}
export interface ITextTypography extends IBaseTypography {type?: 'text';}
export interface IHintTypography extends IBaseTypography {type?: 'hint';}
export interface ICaptionTypography extends IBaseTypography {type?: 'caption';}
