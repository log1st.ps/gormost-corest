import { createLocalVue, shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import {
    CaptionTypographyComponentMixin,
    HintTypographyComponentMixin,
    PrimaryTypographyComponentMixin,
    QuaternaryTypographyComponentMixin, QuinaryTypographyComponentMixin,
    SecondaryTypographyComponentMixin, SenaryTypographyComponentMixin, SeptenaryTypographyComponentMixin,
    TertiaryTypographyComponentMixin, TextTypographyComponentMixin,
} from './TypographyComponentMixin';

describe('Typography Components', () => {
    [
        [PrimaryTypographyComponentMixin, 'primary'],
        [SecondaryTypographyComponentMixin, 'secondary'],
        [TertiaryTypographyComponentMixin, 'tertiary'],
        [QuaternaryTypographyComponentMixin, 'quaternary'],
        [QuinaryTypographyComponentMixin, 'quinary'],
        [SenaryTypographyComponentMixin, 'senary'],
        [SeptenaryTypographyComponentMixin, 'septenary'],
        [TextTypographyComponentMixin, 'text'],
        [HintTypographyComponentMixin, 'hint'],
        [CaptionTypographyComponentMixin, 'caption'],
    ].forEach(([component, type]: any) => {
        describe(`${component.name}`, () => {

            [
                'text',
                'color',
            ].forEach((prop: string) => {
                it(`contains props.${prop}`, () => {
                    const wrapper = shallowMount(component);

                    expect(wrapper.props()).to.contain.keys(prop);
                });
            });

            it(`has type property equal to "${type}"`, () => {
                const wrapper = shallowMount(component);

                // @ts-ignore
                expect(wrapper.vm.type).to.be.equal(type);
            });

        });
    });
});
