import { createLocalVue, shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import { RuntimeException } from '../../../helpers/RuntimeException';
import NavigationComponentMixin from './NavigationComponentMixin';

describe('NavigationComponentMixin', () => {
    ['items', 'value'].forEach((item: string) => {
        it(`contains props.${item}`, () => {
            const wrapper = shallowMount(NavigationComponentMixin);
            expect(wrapper.props()).to.contain.keys(item);
        });
    });

    it('has input as model event and returns expected value', () => {
        [
            [1, 2].map(String),
            [2, 1].map(String),
            [2, 3].map(String),
        ].forEach(([value, expected]) => {
            const wrapper = shallowMount(NavigationComponentMixin, {
                propsData: {
                    value,
                    items: [1, 2, 3].map(String).map(key => ({ key })),
                },
            });

            expect(wrapper.vm).to.contain.keys('onItemClick');

            // @ts-ignore
            wrapper.vm.onItemClick(expected);

            expect(wrapper.emitted().input[0][0]).to.be.equal(expected);
        });
    });

    it(
        // tslint:disable-next-line:max-line-length
        `expects ${RuntimeException.name} to be thrown when no matched value in items`,
        () => {
            [
                ['one', ['two', 'three']],
                ['two', ['one', 'three']],
                ['three', ['one', 'two']],
            ].forEach(([value, items]) => {
                const silentVue = createLocalVue();
                silentVue.config.silent = true;

                expect(
                    () => {
                        shallowMount(NavigationComponentMixin, {
                            propsData: {
                                value,
                                items: (items as []).map(item => ({
                                    key: item,
                                })),
                            },
                            localVue: silentVue,
                        });
                    },
                ).to.throws(RuntimeException);
            });
        },
    );
});
