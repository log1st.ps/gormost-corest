import { Component, Emit, Mixins, Model, Prop, Watch } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import { availableIconTypesList } from '../../../constants/icon/iconTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIEvent from '../../../helpers/documentation/APIEvent';
import APIModel from '../../../helpers/documentation/APIModel';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import { RuntimeException } from '../../../helpers/RuntimeException';
import EmptyRender from '../../../mixins/EmptyRender';
import { INavigation, INavigationItem } from './NavigationComponentMixin.d';

@Component
class Mixin extends VueComponent<INavigation> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        { name: 'default', description: 'Primary content' },
        {
            name: 'item',
            description: 'Content of item',
            // TODO: keys in runtime
            params: [
                'key',
                'text',
                'route',
                'icon',
                'isDisabled',
            ],
        },
    ],
})
@Component
class NavigationComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements INavigation {

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'List of items',
        contains: {
            key: {
                type: PROP_TYPE_STRING,
                isRequired: true,
            },
            text: PROP_TYPE_STRING,
            route: {
                type: [PROP_TYPE_OBJECT, PROP_TYPE_STRING],
                description: 'Route object or string of path',
            },
            icon: {
                type: PROP_TYPE_STRING,
                availableValues: availableIconTypesList,
            },
            isDisabled: PROP_TYPE_BOOLEAN,
        },
    })
    @Prop()
    public items ?: INavigationItem[];

    @APIModel(({
        type: PROP_TYPE_STRING,
        event: 'input',
    }))
    @Model('input')
    public value ?: string;

    @Watch('value', {
        immediate: true,
    })
    public checkIfValueExists(value) {
        if (!value || !Array.isArray(this.items)) {
            return;
        }

        if (this.items.map(({ key }) => key).indexOf(value) === -1) {
            // tslint:disable-next-line:no-console
            throw new RuntimeException('Invalid value: key doesn\'t exist in items');
        }
    }

    @APIEvent({
        name: 'onInput',
        args: [
            {
                name: 'key',
                type: PROP_TYPE_STRING,
            },
        ],
    })
    @Emit('input')
    protected onItemClick(key: string) {
        return key;
    }
}

export default NavigationComponentMixin;
