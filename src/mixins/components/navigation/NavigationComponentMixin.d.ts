import { Route } from 'vue-router';
import {availableIconTypes} from '../../../constants/icon/iconTypesConstants';

export interface INavigation {
    items?: INavigationItem[];
    value?: string;
}

export interface INavigationItem {
    key: string;
    text?: string;
    route?: Route | string;
    icon?: availableIconTypes;
    isDisabled?: boolean;
}
