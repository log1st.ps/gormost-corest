export interface ITabs {
    value?: string;
    onInput?(value: any): void;
}
