import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import { ITabPanel } from './TabPanelComponentMixin.d';

@Component
class Mixin extends VueComponent<ITabPanel> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})

@Component
class TabPanelComponentMixin
    extends Mixins(Mixin)
    implements ITabPanel {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Key',
    })
    @Prop()
    public name ?: string;

}
export default TabPanelComponentMixin;
