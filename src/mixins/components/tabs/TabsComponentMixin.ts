import { Component, Emit, Mixins, Model } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIEvent from '../../../helpers/documentation/APIEvent';
import APIModel from '../../../helpers/documentation/APIModel';
import {PROP_TYPE_STRING} from '../../../helpers/documentation/APIProp';
import { ITabs } from './TabsComponentMixin.d';

@Component
class Mixin extends VueComponent<ITabs> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class TabsComponentMixin
    extends Mixins(Mixin)
    implements ITabs {

    @APIModel({
        type: PROP_TYPE_STRING,
        event: 'input',
    })
    @Model('input')
    public value ?: string;

    @APIEvent({
        name: 'onInput',
    })
    @Emit('input')
    protected onTabClick(value: string) {
        return value;
    }

}
export default TabsComponentMixin;
