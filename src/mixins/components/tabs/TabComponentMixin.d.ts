import { availableIconTypes } from '../../../constants/icon/iconTypesConstants';
import { availableTabTypes } from '../../../constants/tabs/tabTypesConstants';

export interface ITab {
    name: string;
    icon?: availableIconTypes | number;
    label?: string;
    isDisabled?: boolean;
    type?: availableTabTypes;
}
