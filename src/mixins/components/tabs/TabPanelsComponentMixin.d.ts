export interface ITabPanels {
    value?: string;
    isSwipeable?: boolean;

    onShowNext?(): void;
    onShowPrevious?(): void;
}
