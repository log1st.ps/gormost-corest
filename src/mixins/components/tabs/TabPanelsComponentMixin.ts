import {Component, Emit, Mixins, Model, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIProp, {PROP_TYPE_BOOLEAN} from '../../../helpers/documentation/APIProp';
import {ITabPanels} from './TabPanelsComponentMixin.d';

@Component
// @ts-ignore
class Mixin extends VueComponent<ITabPanels> {
}

// tslint:disable-next-line:max-classes-per-file
@Component
class TabPanelsComponentMixin
    extends Mixins(Mixin)
    implements ITabPanels {

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is swipeable',
        defaultValue: true,
    })
    @Prop()
    public isSwipeable ?: boolean;

    @Emit('showPrevious')
    protected showPrevious() {
        // something
    }

    @Emit('showNext')
    protected showNext() {
        // something
    }
}

export default TabPanelsComponentMixin;
