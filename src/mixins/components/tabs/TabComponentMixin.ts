import {Component, Emit, Mixins, Prop} from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import { availableIconTypes, availableIconTypesList } from '../../../constants/icon/iconTypesConstants';
import {availableTabTypes, availableTabTypesList, TAB_TYPE_DEFAULT} from '../../../constants/tabs/tabTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_ANY, PROP_TYPE_BOOLEAN, PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import { ITab } from './TabComponentMixin.d';

@Component
class Mixin extends VueComponent<ITab> {}

@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Instead of label and icon',
        },
    ],
})
// tslint:disable-next-line:max-classes-per-file
@Component
class TabComponentMixin
    extends Mixins(Mixin)
    implements ITab {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Name',
    })
    @Prop()
    public name !: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        availableValues: availableIconTypesList,
        description: 'Icon',
    })
    @Prop()
    public icon ?: availableIconTypes | number;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Label',
    })
    @Prop()
    public label ?: string;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is Disabled',
    })
    @Prop()
    public isDisabled ?: boolean;

    @APIProp({
        type: PROP_TYPE_ANY,
        description: 'type',
        defaultValue: TAB_TYPE_DEFAULT,
        availableValues: availableTabTypesList,
    })
    @Prop({default: () => TAB_TYPE_DEFAULT})
    public type ?: availableTabTypes;

    public onClick() {
        if (this.isDisabled) {
            return;
        }
        return this.$emit('click', this.name);
    }
}

export default TabComponentMixin;
