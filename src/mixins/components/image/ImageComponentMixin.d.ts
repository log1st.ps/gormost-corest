import {
    availableBgTypes,
} from '../../../constants/image/imageBackgroundTypesConstants';
import {
    availableImageTypes,
} from '../../../constants/image/imageTypesConstants';

export interface IImage {
    src: string;
    alt?: string;
    type?: availableImageTypes;
    backgroundType?: availableBgTypes;
    height?: number;
    width?: number;
}
