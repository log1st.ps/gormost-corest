import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import ImageComponentMixin from './ImageComponentMixin';

describe('ImageComponentMixin', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallowMount(ImageComponentMixin);
    });
    [
        'src',
        'alt',
        'type',
        'backgroundType',
        'height',
        'width',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

});
