﻿import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import {
    availableBgTypes,
    availableBgTypesList,
} from '../../../constants/image/imageBackgroundTypesConstants';
import {
  availableImageTypes,
  availableImageTypesList,
  IMAGE_TYPE_IMG,
} from '../../../constants/image/imageTypesConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, {
  PROP_TYPE_NUMBER,
  PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {
    IImage,
} from './ImageComponentMixin.d';

@Component
class Mixin extends VueComponent<IImage> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        { name: 'default', description: 'If the picture has not loaded yet' },
        { name: 'placeholder', description: 'Triggers when image load' },
        { name: 'error', description: 'Picture failed to load' },
    ],
})
@Component
class ImageComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IImage {
    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Image source',
    })
    @Prop()
    public src !: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Alternative description',
    })
    @Prop()
    public alt ?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Type of image',
        availableValues: availableImageTypesList,
    })
    @Prop({ default: IMAGE_TYPE_IMG })
    public type ?: availableImageTypes;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Background size of block type',
        availableValues: availableBgTypesList,
    })
    @Prop()
    public backgroundType ?: availableBgTypes;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Height in px',
    })
    @Prop()
    public height ?: number;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Width in px',
    })
    @Prop()
    public width ?: number;

}

export default ImageComponentMixin;
