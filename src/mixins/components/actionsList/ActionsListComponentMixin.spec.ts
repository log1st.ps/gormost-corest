import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import { ACTIONS_LIST_ALIGN_CENTER } from '../../../constants/actionsList/actionsListAlignsConstants';
import ActionsListComponentMixin from './ActionsListComponentMixin';

describe('ActionsListComponentMixin', () => {
    [
        'actions',
        'align',
    ].forEach((item: string) => {
        it(`contains props.${item}`, () => {
            const wrapper = shallowMount(ActionsListComponentMixin);
            expect(wrapper.props()).to.contain.keys(item);
        });
    });

    it(`has props.item default value equal to "${ACTIONS_LIST_ALIGN_CENTER}"`, () => {
        const wrapper = shallowMount(ActionsListComponentMixin);
        expect(wrapper.vm.align).to.be.equal(ACTIONS_LIST_ALIGN_CENTER);
    });
});
