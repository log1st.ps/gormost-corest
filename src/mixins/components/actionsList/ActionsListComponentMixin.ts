import {Component, Mixins, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import {
    ACTIONS_LIST_ALIGN_CENTER,
    availableActionsListAligns,
    availableActionsListAlignsList,
} from '../../../constants/actionsList/actionsListAlignsConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_FUNCTION,
    PROP_TYPE_NUMBER,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {IActionButton, IActionsList} from './ActionsListComponentMixin.d';
import {availableActionsListDirections} from '../../../constants/actionsList/actionsListDirectionContstants';
import {availableRowDirectionsList, ROW_DIRECTION_ROW} from '../../../constants/grid/rowDirectionsConstants';

@Component
class Mixin extends VueComponent<IActionsList> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({

})
@Component
class ActionsListComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IActionsList {

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Array of actions list inherits ButtonElement[] with onClick handler in each',
        contains: {
            'key': PROP_TYPE_STRING,
            '...IButtonElementMixin': {
                type: PROP_TYPE_OBJECT,
                description: 'All props from ButtonElementMixin',
            },
            'onClick': {
                type: PROP_TYPE_FUNCTION,
                description: 'Click handler',
            },
        },
    })
    @Prop()
    public actions?: IActionButton[];

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Align of actions',
        defaultValue: ACTIONS_LIST_ALIGN_CENTER,
        availableValues: availableActionsListAlignsList,
    })
    @Prop({ default: () => ACTIONS_LIST_ALIGN_CENTER })
    public align?: availableActionsListAligns;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Gap between buttons',
    })
    @Prop()
    public gap?: number;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Direction',
        defaultValue: ROW_DIRECTION_ROW,
        availableValues: availableRowDirectionsList,
    })
    @Prop({default: () => ROW_DIRECTION_ROW})
    public direction?: availableActionsListDirections;
}

export default ActionsListComponentMixin;
