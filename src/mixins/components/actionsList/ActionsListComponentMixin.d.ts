import { IButton } from '../../elements/button/ButtonElementMixin.d';
import {availableActionsListAligns} from '../../../constants/actionsList/actionsListAlignsConstants';
import {availableActionsListDirections} from '../../../constants/actionsList/actionsListDirectionContstants';

export interface IActionButton extends IButton {
    key: string;
    onClick?(): void;
}

export interface IActionsList {
    actions?: IActionButton[];
    align?: availableActionsListAligns;
    gap?: number;
    direction?: availableActionsListDirections;
}
