import { Route } from 'vue-router';

export interface IListElement {
    key: string;
    route?: Route | string;
    title?: string;
    onClick?(): void;
}

export interface IList {
    title?: string;
    items?: IListElement[];
}
