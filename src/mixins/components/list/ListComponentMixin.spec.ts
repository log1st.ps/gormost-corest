import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import ListComponentMixin from './ListComponentMixin';

describe('ListComponentMixin', () => {
    [
        'title',
        'items',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(ListComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
