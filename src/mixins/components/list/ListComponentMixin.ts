import {Component, Mixins, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_FUNCTION,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {IList, IListElement} from './ListComponentMixin.d';

@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
        {
            name: 'item',
            description: 'Any item content',
            params: {
                key: PROP_TYPE_STRING,
                route: {
                    type: [PROP_TYPE_OBJECT, PROP_TYPE_STRING],
                    description: 'Route object or string',
                },
                onClick: {
                    type: PROP_TYPE_FUNCTION,
                    description: 'Click handler',
                },
            },
        },
        {
            // tslint:disable-next-line:no-invalid-template-strings
            name: '${key}_item',
            description: 'Custom item by key content',
        },
    ],
})
@Component
class Mixin extends VueComponent<IList> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class ListComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IList {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Title of item',
    })
    @Prop()
    public title ?: string;

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'List of items',
        contains: {
            key: PROP_TYPE_STRING,
            route: {
                type: [PROP_TYPE_OBJECT, PROP_TYPE_STRING],
                description: 'Route object or string',
            },
            onClick: {
                type: PROP_TYPE_FUNCTION,
                description: 'Click handler',
            },
        },
    })
    @Prop()
    public items ?: IListElement[];
}

export default ListComponentMixin;
