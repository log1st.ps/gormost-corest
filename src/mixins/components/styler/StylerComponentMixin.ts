import {VueComponent} from 'vue-tsx-helper';
import {Component, Prop} from 'vue-property-decorator';
import {IStyler} from './StylerComponentMixin.d';
import APIProp, {PROP_TYPE_NUMBER, PROP_TYPE_STRING} from '../../../helpers/documentation/APIProp';
import {availableStylerColorsList} from '../../../constants/styler/availableStylerColorsConstants';

@Component
class StylerComponentMixin
    extends VueComponent<IStyler>
    implements IStyler {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Background color',
        availableValues: availableStylerColorsList,
    })
    @Prop()
    public background;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Font color',
        availableValues: availableStylerColorsList,
    })
    @Prop()
    public color;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Height',
    })
    @Prop()
    public height?: number;
}

export default StylerComponentMixin;
