import {availableStylerColors} from "../../../constants/styler/availableStylerColorsConstants";

export interface IStyler {
    background?: availableStylerColors;
    color?: availableStylerColors;
    height?: number;
}