import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import ModalComponentMixin from './ModalComponentMixin';

describe('ModalComponentMixin', () => {
    [
        'value',
        'isBackdropClosable',
        'portalName',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(ModalComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
