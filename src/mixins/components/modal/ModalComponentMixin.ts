import {Component, Emit, Mixins, Model, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIModel from '../../../helpers/documentation/APIModel';
import APIProp, {PROP_TYPE_BOOLEAN, PROP_TYPE_NUMBER, PROP_TYPE_STRING} from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {IModal} from './ModalComponentMixin.d';
import {PORTAL_MODAL} from '../../../constants/portal/portalNames';

@Component
class Mixin extends VueComponent<IModal> {
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class ModalComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IModal {

    @APIModel({
        type: PROP_TYPE_BOOLEAN,
        event: 'input',
    })
    @Model('input')
    public value?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'If true - click on backdrop will emit close',
    })
    @Prop()
    public isBackdropClosable ?: boolean;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Portal name',
    })
    @Prop({default: () => PORTAL_MODAL})
    public portalName ?: string;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Max width of content container',
    })
    @Prop()
    public maxWidth ?: number;

    @Emit('input')
    protected setValue(value: boolean) {
        return value;
    }

    protected toggleValue() {
        this.setValue(!this.value);
    }
}

export default ModalComponentMixin;
