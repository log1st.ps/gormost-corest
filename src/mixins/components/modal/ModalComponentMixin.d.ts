export interface IModal {
    value?: boolean;
    isBackdropClosable?: boolean;
    portalName?: string;
    maxWidth?: number;
}
