import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import AccordionComponentMixin from './AccordionComponentMixin';

describe('AccordionComponentMixin', () => {
    [
        'title',
        'value',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(AccordionComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it('emits input on click', () => {
        [
            true,
            false,
        ].forEach((value: boolean) => {
            const wrapper = shallowMount(AccordionComponentMixin, {
                propsData: {
                    value,
                },
            });

            // @ts-ignore
            wrapper.vm.onClick();

            expect(wrapper.emitted()).to.contain.keys('input');

            expect(wrapper.emitted().input[0][0]).to.be.equal(!value);
        });
    });
});
