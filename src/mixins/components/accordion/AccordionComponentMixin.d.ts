export interface IAccordion {
    title?: string;
    value?: boolean;
}
