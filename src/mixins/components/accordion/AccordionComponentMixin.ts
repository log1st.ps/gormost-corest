import { Component, Emit, Mixins, Model, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIEvent from '../../../helpers/documentation/APIEvent';
import APIModel from '../../../helpers/documentation/APIModel';
import APIProp, { PROP_TYPE_BOOLEAN, PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import { IAccordion } from './AccordionComponentMixin.d';

@Component
// @ts-ignore
class Mixin extends VueComponent<IAccordion> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'header',
            description: 'Instead of title',
        },
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class AccordionComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IAccordion {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Title',
    })
    @Prop()
    public title ?: string;

    @APIModel({
        type: PROP_TYPE_BOOLEAN,
        description: 'Value',
        event: 'input',
        defaultValue: true,
    })
    @Model('input', { default: () => true })
    public value ?: boolean;

    @APIEvent({
        name: 'input',
    })
    @Emit('input')
    protected onClick() {
        return !this.value;
    }
}
export default AccordionComponentMixin;
