import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIProp, { PROP_TYPE_BOOLEAN } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import { ICard } from './CardComponentMixin.d';

@Component
class Mixin extends VueComponent<ICard> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class CardComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements ICard {

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if card has shadow',
    })
    @Prop()
    public hasShadow?: boolean;
}

export default CardComponentMixin;
