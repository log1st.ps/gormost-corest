import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import CardComponentMixin from './CardComponentMixin';

describe('CardComponentMixin', () => {
    [
        'hasShadow',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(CardComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
