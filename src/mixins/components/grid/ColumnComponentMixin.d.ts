import {availableRowAligns} from "../../../constants/grid/rowAlignsConstants";

export interface IColumn {
    size?: number | string;
    isGrow?: boolean;
    isShrink?: boolean;
    gap?: number;
    align?: availableRowAligns;
}