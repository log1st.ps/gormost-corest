import {VueComponent} from 'vue-tsx-helper';
import {IRow} from './RowComponentMixin.d';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import EmptyRender from '../../EmptyRender';
import {
    availableRowAligns,
    availableRowAlignsList,
    ROW_ALIGN_CENTER,
} from '../../../constants/grid/rowAlignsConstants';
import APIProp, {PROP_TYPE_BOOLEAN, PROP_TYPE_NUMBER, PROP_TYPE_STRING} from '../../../helpers/documentation/APIProp';
import {
    availableRowJustifies,
    availableRowJustifiesList,
    ROW_JUSTIFY_START,
} from '../../../constants/grid/rowJustifiesConstants';
import {
    availableRowDirections,
    availableRowDirectionsList,
    ROW_DIRECTION_ROW,
} from '../../../constants/grid/rowDirectionsConstants';
import APIComponent from '../../../helpers/documentation/APIComponent';

@Component
class Mixin extends VueComponent<IRow> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class RowComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IRow {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Align',
        defaultValue: ROW_ALIGN_CENTER,
        availableValues: availableRowAlignsList,
    })
    @Prop({default: () => ROW_ALIGN_CENTER})
    public align ?: availableRowAligns;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Justify',
        defaultValue: ROW_JUSTIFY_START,
        availableValues: availableRowJustifiesList,
    })
    @Prop({default: () => ROW_JUSTIFY_START})
    public justify ?: availableRowJustifies;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Direction',
        defaultValue: ROW_DIRECTION_ROW,
        availableValues: availableRowDirectionsList,
    })
    @Prop({default: () => ROW_DIRECTION_ROW})
    public direction ?: availableRowDirections;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Allows to wrap cells',
    })
    @Prop()
    public isWrap ?: boolean;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Gap between cells',
    })
    @Prop()
    public gap ?: number;
}

export default RowComponentMixin;
