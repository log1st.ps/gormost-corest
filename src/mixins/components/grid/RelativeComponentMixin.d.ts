import {dim} from '../../../constants/grid/absolutePositionsConstants';

export interface IRelative {
    offset?: dim | [dim, dim] | [dim, dim, dim] | [dim, dim, dim, dim]
}