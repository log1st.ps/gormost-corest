import {availableRowAligns} from "../../../constants/grid/rowAlignsConstants";
import {availableRowJustifies} from "../../../constants/grid/rowJustifiesConstants";
import {availableRowDirections} from "../../../constants/grid/rowDirectionsConstants";

export interface IRow {
    align?: availableRowAligns;
    justify?: availableRowJustifies;
    direction?: availableRowDirections;
    isWrap?: boolean;
    gap?: number;
}