import {VueComponent} from 'vue-tsx-helper';

import {IAbsolute} from './AbsoluteComponentMixin.d';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import EmptyRender from '../../EmptyRender';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_NUMBER,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import {
    availableAbsoluteAligns,
    availableAbsoluteAlignsList,
    availableAbsolutePositions,
    availableAbsolutePositionsList,
    dim,
} from '../../../constants/grid/absolutePositionsConstants';

@Component
class Mixin extends VueComponent<IAbsolute> {}


// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class AbsoluteComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IAbsolute {

    @APIProp({
        type: [PROP_TYPE_NUMBER, PROP_TYPE_ARRAY],
        description: 'Width of offset or array of widths as in css',
    })
    @Prop()
    public offset?: dim | [dim, dim] | [dim, dim, dim] | [dim, dim, dim, dim];

    @APIProp({
        type: [PROP_TYPE_NUMBER, PROP_TYPE_STRING],
        description: 'Position in px or preset value or in [preset value, offset] array',
        availableValues: ['Any number', ...availableAbsolutePositionsList],
    })
    @Prop()
    public position ?: number | availableAbsolutePositions | [availableAbsolutePositions, number];

    @APIProp({
        type: [PROP_TYPE_NUMBER, PROP_TYPE_STRING],
        description: 'Align in px or preset value or in [preset value, offset] array',
        availableValues: ['Any number', null, ...availableAbsoluteAlignsList],
    })
    @Prop()
    public align ?: number | availableAbsoluteAligns | [availableAbsoluteAligns, number];

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: ' Is fixed',
    })
    @Prop()
    public isFixed?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is sticky',
    })
    @Prop()
    public isSticky?: boolean;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Z-index',
    })
    @Prop()
    public zIndex?: number;
}

export default AbsoluteComponentMixin;
