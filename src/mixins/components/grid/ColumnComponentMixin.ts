import {VueComponent} from 'vue-tsx-helper';
import {IColumn} from './ColumnComponentMixin.d';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import EmptyRender from '../../EmptyRender';
import APIProp, {PROP_TYPE_BOOLEAN, PROP_TYPE_NUMBER, PROP_TYPE_STRING} from '../../../helpers/documentation/APIProp';
import APIComponent from '../../../helpers/documentation/APIComponent';
import {availableRowAligns} from '../../../constants/grid/rowAlignsConstants';

@Component
class Mixin extends VueComponent<IColumn> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class ColumnComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IColumn {

    @APIProp({
        type: [PROP_TYPE_NUMBER, PROP_TYPE_STRING],
        description: 'Width of cell',
    })
    @Prop()
    public size ?: number | string;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is cell growing',
    })
    @Prop()
    public isGrow ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is cell shrinking',
    })
    @Prop()
    public isShrink ?: boolean;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Half of gap between cells',
    })
    @Prop()
    public gap ?: number;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Self align',
    })
    @Prop()
    public align ?: availableRowAligns;

}

export default ColumnComponentMixin;
