import {VueComponent} from 'vue-tsx-helper';
import {IRelative} from './RelativeComponentMixin.d';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import EmptyRender from '../../EmptyRender';
import APIProp, {PROP_TYPE_ARRAY, PROP_TYPE_NUMBER} from '../../../helpers/documentation/APIProp';
import APIComponent from '../../../helpers/documentation/APIComponent';
import {dim} from '../../../constants/grid/absolutePositionsConstants';

@Component
class Mixin extends VueComponent<IRelative> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class RelativeComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IRelative {

    @APIProp({
        type: [PROP_TYPE_NUMBER, PROP_TYPE_ARRAY],
        description: 'any',
    })
    @Prop()
    public offset?: dim | [dim, dim] | [dim, dim, dim] | [dim, dim, dim, dim];

}

export default RelativeComponentMixin;
