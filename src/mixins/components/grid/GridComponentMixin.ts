import {VueComponent} from 'vue-tsx-helper';
import {Component, Mixins, Prop} from 'vue-property-decorator';
import EmptyRender from '../../EmptyRender';
import {IGrid} from './GridComponentMixin.d';
import APIProp, {PROP_TYPE_NUMBER} from '../../../helpers/documentation/APIProp';
import APIComponent from '../../../helpers/documentation/APIComponent';

@Component
class Mixin extends VueComponent<IGrid> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
    ],
})
@Component
class GridComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IGrid {

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Space between columns',
    })
    @Prop()
    public gap ?: number;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: `Number of similar columns or array of columns' width`,
    })
    @Prop()
    public columns ?: number | number[];

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: `Number of similar rows or array of columns' width`,
    })
    @Prop()
    public rows ?: number | number[];
}

export default GridComponentMixin;
