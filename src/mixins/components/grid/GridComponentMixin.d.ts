export interface IGrid {
    gap?: number;
    columns?: number | number[];
    rows?: number | number[];
}