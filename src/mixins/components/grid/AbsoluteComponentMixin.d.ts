import {
    availableAbsoluteAligns,
    availableAbsolutePositions,
    dim,
} from '../../../constants/grid/absolutePositionsConstants';

export interface IAbsolute {
    offset?: dim | [dim, dim] | [dim, dim, dim] | [dim, dim, dim, dim];
    position?: number | availableAbsolutePositions | [availableAbsolutePositions, number];
    align?: number | availableAbsoluteAligns | [availableAbsoluteAligns, number];
    isFixed?: boolean;
    isSticky?: boolean;
    zIndex?: number;
}
