import { IActionsList } from '../actionsList/ActionsListComponentMixin.d';

export interface IDialog {
    title ?: string;
    actions ?: IActionsList;
    onClose?(): void;
}
