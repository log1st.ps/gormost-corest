import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import DialogComponentMixin from './DialogComponentMixin';

describe('DialogComponentMixin', () => {
    [
        'title',
        'actions',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(DialogComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
