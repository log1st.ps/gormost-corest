import {Component, Emit, Mixins, Prop} from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_OBJECT, PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import { IActionsList } from '../actionsList/ActionsListComponentMixin.d';
import EmptyRender from '../../../mixins/EmptyRender';
import { IDialog } from './DialogComponentMixin.d';

@Component
class Mixin extends VueComponent<IDialog> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Primary content',
        },
        {
            name: 'header',
            description: 'Instead of title',
        },
        {
            name: 'footer',
            description: 'Instead of footer',
        },
    ],
})
@Component
class DialogComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IDialog {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Title',
    })
    @Prop()
    public title?: string;

    @APIProp({
        type: PROP_TYPE_OBJECT,
        description: 'ActionsList configuration object',
    })
    @Prop()
    public actions?: IActionsList;

    @Emit('close')
    protected onCloseClick() {
        // something
    }
}

export default DialogComponentMixin;
