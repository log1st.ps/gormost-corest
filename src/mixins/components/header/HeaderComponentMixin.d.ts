export interface IUser {
    avatarPath?: string;
    firstName?: string;
    lastName?: string;
    middleName?: string;
}

export interface IHeader {
    logoPath?: string;
    withNotifications?: boolean;
    hasNotifications?: boolean;
    user?: IUser;
    isNotificationsListActive?: boolean;
    isUserControlActive?: boolean;
}
