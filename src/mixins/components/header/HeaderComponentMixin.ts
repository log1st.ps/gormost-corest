import { Component, Emit, Mixins, Model, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIEvent from '../../../helpers/documentation/APIEvent';
import APIModel from '../../../helpers/documentation/APIModel';
import APIProp, { PROP_TYPE_BOOLEAN, PROP_TYPE_OBJECT, PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import { IHeader, IUser } from './HeaderComponentMixin.d';

@Component
class Mixin extends VueComponent<IHeader> {
}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Space between logo and side',
        },
        {
            name: 'logo',
            description: 'Instead of logo',
            params: {
                logoPath: PROP_TYPE_STRING,
            },
        },
        {
            name: 'side',
            description: 'Instead of notification and user',
            params: {
                withNotifications: PROP_TYPE_BOOLEAN,
                hasNotifications: PROP_TYPE_BOOLEAN,
                user: {
                    type: PROP_TYPE_OBJECT,
                    contains: {
                        avatarPath: PROP_TYPE_STRING,
                        firstName: PROP_TYPE_STRING,
                        lastName: PROP_TYPE_STRING,
                        middleName: PROP_TYPE_STRING,
                    },
                },
            },
        },
    ],
})
@Component
class HeaderComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IHeader {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Link to logo image',
    })
    @Prop()
    public logoPath ?: string;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if header should contain notifications button',
    })
    @Prop()
    public withNotifications ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if notifications button has visual information about existing notification',
    })
    @Prop()
    public hasNotifications ?: boolean;

    @APIProp({
        type: PROP_TYPE_OBJECT,
        description: 'User information object',
        contains: {
            avatarPath: PROP_TYPE_STRING,
            firstName: PROP_TYPE_STRING,
            lastName: PROP_TYPE_STRING,
            middleName: PROP_TYPE_STRING,
        },
    })
    @Prop()
    public user ?: IUser;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Toggles notifications visibility',
    })
    @Prop()
    public isNotificationsListActive ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Toggles user info visibility',
    })
    @Prop()
    public isUserControlActive ?: boolean;

    @APIEvent({
        name: 'notificationTriggerClick',
        args: [
            {
                name: 'value',
                type: PROP_TYPE_BOOLEAN,
            },
        ],
    })
    @Emit('notificationTriggerClick')
    protected onNotificationTriggerClick() {
        return !this.isNotificationsListActive;
    }

    @APIEvent({
        name: 'onUserClick',
        args: [
            {
                name: 'value',
                type: PROP_TYPE_BOOLEAN,
            },
        ],
    })
    @Emit('userClick')
    protected onUserClick() {
        return !this.isUserControlActive;
    }

    @APIEvent({
        name: 'onLogoClick',
    })
    @Emit('logoClick')
    protected onLogoClick() {
        // something
    }
}

export default HeaderComponentMixin;
