import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import HeaderComponentMixin from './HeaderComponentMixin';

describe('HeaderComponentMixin', () => {
    [
        'logoPath',
        'withNotifications',
        'hasNotifications',
        'user',
        'value',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(HeaderComponentMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it('emits input on user click', () => {
        [false, true].forEach((value: boolean) => {
            const wrapper = shallowMount(HeaderComponentMixin, {
                propsData: {
                    value,
                },
            });

            // @ts-ignore
            wrapper.vm.onUserClick();

            expect(wrapper.emitted().input[0][0]).to.be.equal(!value);
        });
    });
});
