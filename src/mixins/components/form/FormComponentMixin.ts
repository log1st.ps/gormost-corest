import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_STRING } from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import { IForm } from './FormComponentMixin.d';

@Component
class Mixin extends VueComponent<IForm> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Content of form',
        },
    ],
})
@Component
class FormComponentMixin
    extends Mixins(Mixin, EmptyRender)
    implements IForm {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Unique id for third-parties',
    })
    @Prop()
    public id?: string;
}

export default FormComponentMixin;
