import {Component, Emit, Mixins, Model, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIEvent from '../../../../helpers/documentation/APIEvent';
import APIModel from '../../../../helpers/documentation/APIModel';
import APIProp, {PROP_TYPE_ANY, PROP_TYPE_BOOLEAN, PROP_TYPE_STRING} from '../../../../helpers/documentation/APIProp';
import EmptyRender from '../../../../mixins/EmptyRender';
import {ICommonFormElement} from './CommonFormElementMixin.d';
import {availableIconTypes, availableIconTypesList} from '../../../../constants/icon/iconTypesConstants';

@Component
class Mixin extends VueComponent<ICommonFormElement> {}

// tslint:disable-next-line:max-classes-per-file
@Component
class CommonFormElementMixin
    extends Mixins(Mixin, EmptyRender)
    implements ICommonFormElement {

    @APIProp(({
        type: PROP_TYPE_STRING,
        description: 'Label',
    }))
    @Prop()
    public placeholder ?: string;

    @APIModel({
        type: PROP_TYPE_STRING,
        event: 'input',
    })
    @Model('input')
    public value ?: any;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set state to disabled',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public isDisabled ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set state to readonly',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public isReadonly ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set state to valid',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public isValid ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set state to invalid',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public isInvalid ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Toggle ability to clear value',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public isClearable ?: boolean;

    @APIProp({
        type: PROP_TYPE_ANY,
        description: 'Clear value',
        defaultValue: null,
    })
    @Prop({ default: () => null })
    public clearValue ?: any;

    @APIProp({
        type: PROP_TYPE_ANY,
        description: 'Set if element has appearance',
        defaultValue: false,
    })
    @Prop({ default: () => false })
    public hasAppearance ?: boolean;

    @Prop()
    public before ?: string | availableIconTypes;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Content before field',
        availableValues: availableIconTypesList,
    })
    @Prop()
    public after ?: string | availableIconTypes;

    @APIEvent({
        name: 'input',
        args: [
            {
                name: 'value',
                type: PROP_TYPE_ANY,
            },
        ],
        description: 'Model update event',
    })
    @Emit('input')
    public updateValue(value: any) {
        return value;
    }
}

export default CommonFormElementMixin;
