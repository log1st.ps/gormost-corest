import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import FieldFormElementMixin from './FieldFormElementMixin';

describe('FieldFormElementMixin', () => {
    [
        'title',
        'preHint',
        'hint',
        'error',
        'isValidated',
    ].forEach((prop: string) => {
        const wrapper = shallowMount(FieldFormElementMixin);

        expect(wrapper.props()).to.contain.keys(prop);
    });
});
