import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_STRING,
} from '../../../../../helpers/documentation/APIProp';
import EmptyRender from '../../../../../mixins/EmptyRender';
import { IFieldFormElement } from './FieldFormElementMixin.d';

@Component
class Mixin extends VueComponent<IFieldFormElement> {}

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'title',
            description: 'Title content',
        },
        {
            name: 'preHint',
            description: 'Pre hint content',
        },
        {
            name: 'hint',
            description: 'Hint content',
        },
        {
            name: 'error',
            description: 'Error content',
            params: {
                messages: {
                    type: PROP_TYPE_ARRAY,
                    description: 'Array of messages',
                },
            },
        },
    ],
})
@Component
class FieldFormElementMixin
    extends Mixins(Mixin, EmptyRender)
    implements IFieldFormElement {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Title',
    })
    @Prop()
    public title?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Pre hint',
    })
    @Prop()
    public preHint?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Hint',
    })
    @Prop()
    public hint?: string;

    @APIProp({
        type: [PROP_TYPE_STRING, PROP_TYPE_ARRAY],
        description: 'Error or array of errors',
    })
    @Prop()
    public error?: string|string[];
}

export default FieldFormElementMixin;
