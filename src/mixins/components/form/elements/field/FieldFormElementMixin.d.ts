export interface IFieldFormElement {
    title?: string;
    preHint?: string;
    hint?: string;
    error?: string | string[];
}
