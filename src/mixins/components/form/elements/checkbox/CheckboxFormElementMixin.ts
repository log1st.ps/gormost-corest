import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import {
    availableFormCheckboxTypes,
    availableFormCheckboxTypesList,
    CHECKBOX_TYPE_CHECKBOX,
} from '../../../../../constants/form/checkbox/formCheckboxTypesConstants';
import APIComponent from '../../../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_BOOLEAN, PROP_TYPE_STRING } from '../../../../../helpers/documentation/APIProp';
import CommonFormElementMixin from '../CommonFormElementMixin';
import {ICommonFormElement, IFieldType} from '../CommonFormElementMixin.d';
import { ICheckboxFormElement } from './CheckboxFormElementMixin.d';

@Component
// @ts-ignore
class Mixin extends VueComponent<ICheckboxFormElement> {}

export const FIELD_TYPE_CHECKBOX: IFieldType = 'checkbox';

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Instead of label',
        },
    ],
})
@Component
class CheckboxFormElementMixin
    extends Mixins(Mixin, CommonFormElementMixin)
    implements ICheckboxFormElement, ICommonFormElement {

    @APIProp({
        type: PROP_TYPE_STRING,
        defaultValue: CHECKBOX_TYPE_CHECKBOX,
        availableValues: availableFormCheckboxTypesList,
    })
    @Prop({ default: () => CHECKBOX_TYPE_CHECKBOX })
    public type ?: availableFormCheckboxTypes;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'True value',
    })
    @Prop({ default: () => true })
    public trueValue ?: any;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'False value',
    })
    @Prop({ default: () => false })
    public falseValue ?: any;
}

export default CheckboxFormElementMixin;
