import {ICommonFormElement} from '../CommonFormElementMixin.d';

export interface ICheckboxFormElement extends ICommonFormElement {
    type?: availableFormCheckboxTypes;
    trueValue?: any;
    falseValue?: any;
}
