import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import { CHECKBOX_TYPE_CHECKBOX } from '../../../../../constants/form/checkbox/formCheckboxTypesConstants';
import CheckboxFormElementMixin from './CheckboxFormElementMixin';

describe('CheckboxFormElementMixin', () => {
    [
        'type',
        'trueValue',
        'falseValue',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(CheckboxFormElementMixin);
            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it(`has prop type default value equal to "${CHECKBOX_TYPE_CHECKBOX}"`, () => {
        const wrapper = shallowMount(CheckboxFormElementMixin);

        expect(wrapper.vm.type).to.be.equal(CHECKBOX_TYPE_CHECKBOX);
    });

    it('has prop trueValue default value equal to true', () => {
        const wrapper = shallowMount(CheckboxFormElementMixin);

        expect(wrapper.vm.trueValue).to.be.equal(true);
    });

    it('has prop falseValue default value equal to false', () => {
        const wrapper = shallowMount(CheckboxFormElementMixin);

        expect(wrapper.vm.falseValue).to.be.equal(false);
    });
});
