import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import SelectFormElementMixin from './SelectFormElementMixin';

describe('SelectFormElementMixin', () => {
    [
        'options',
        'isMultiple',
        'isAutocomplete',
        'queryPlaceholder',
        'queryTimeout',
        'portalName',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(SelectFormElementMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
