import {ICommonFormElement} from '../CommonFormElementMixin.d';

export interface ISelectOption {
    key: string | number;
    text?: string | null;
    isSelected?: boolean;
    [key: string]: any;
}

export interface ISelectFormElement extends ICommonFormElement {
    options?: Array<ISelectOption | string | number>;
    isMultiple?: boolean;
    withQuery?: boolean;
    queryPlaceholder?: string;
    queryTimeout?: number;
    portalName?: string;
}
