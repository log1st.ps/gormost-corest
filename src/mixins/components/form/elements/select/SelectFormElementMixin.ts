import {Component, Emit, Mixins, Prop, Watch} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIComponent from '../../../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ANY,
    PROP_TYPE_ARRAY,
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_NUMBER,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../../../helpers/documentation/APIProp';
import waitForTicks from '../../../../../helpers/waitForTicks';
import CommonFormElementMixin from '../CommonFormElementMixin';
import {IFieldType} from '../CommonFormElementMixin.d';
import {ISelectFormElement, ISelectOption} from './SelectFormElementMixin.d';
import {PORTAL_SELECT} from '../../../../../constants/portal/portalNames';

@Component
class Mixin extends VueComponent<ISelectFormElement> {}

export const FIELD_TYPE_SELECT: IFieldType = 'select';

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'Instead of content of option',
            params: {
                record: {
                    type: PROP_TYPE_OBJECT,
                    contains: {
                        'key': PROP_TYPE_STRING,
                        'text': PROP_TYPE_STRING,
                        '...buttonElement': {
                            type: PROP_TYPE_ANY,
                            description: 'Other props from passed option',
                        },
                    },
                },
                index: PROP_TYPE_NUMBER,
            },
        },
        {
            name: 'empty',
            description: 'If there\'s no options',
        },
    ],
})
@Component
class SelectFormElementMixin
    extends Mixins(Mixin, CommonFormElementMixin)
    implements ISelectFormElement {

    get computedOptions(): ISelectOption[] {
        return (Array.isArray(this.options) ? this.options : []).map(
            (option: ISelectOption | string | number) => ({
                key: (typeof option === 'object') ? option.key : option,
                text: (typeof option === 'object') ? (option.text || String(option.key)) : String(option),
                isSelected: (
                    Array.isArray(this.value) ? this.value : [this.value]
                ).indexOf(((typeof option === 'object') ? option.key : option)) > -1,
            }),
        );
    }

    get computedValue(): Array<string | number> {
        return this.computedOptions
            .filter(({ key }) => (Array.isArray(this.value) ? this.value : [this.value]).indexOf(key) > -1)
            .map(({ key }) => key);
    }

    get computedValueOptions() {
        return this.computedOptions.filter(({key}) => this.computedValue.indexOf(key) > -1).map((option) => ({
            ...option,
        }));
    }

    get isEmpty(): boolean {
        return (Array.isArray(this.value) ? this.value : [this.value])
            .filter(Boolean)
            .length === 0;
    }

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Options list',
        contains: {
            key: PROP_TYPE_STRING,
            text: PROP_TYPE_STRING,
        },
    })
    @Prop({ default: () => [] })
    public options ?: ISelectOption[];

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if select is multiple',
    })
    @Prop()
    public isMultiple ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if select is autocomplete',
    })
    @Prop()
    public isAutocomplete ?: boolean;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Text on autocomplete input placeholder',
    })
    @Prop()
    public queryPlaceholder ?: string;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Timeout between request emitted',
        defaultValue: 250,
    })
    @Prop({ default: () => 250 })
    public queryTimeout ?: number;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Portal name',
    })
    @Prop({default: () => PORTAL_SELECT})
    public portalName ?: string;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'If select has filter',
    })
    @Prop()
    public withQuery ?: boolean;

    protected isOptionsContainerVisible ?: boolean;

    protected queryString: string = '';

    protected timeoutInstance ?: number;

    @Watch('queryString', {
        immediate: true,
    })
    protected async handleQueryUpdate() {
        await waitForTicks(this);

        if (this.queryString.length < 3) {
            return;
        }

        if (this.timeoutInstance) {
            clearTimeout(this.timeoutInstance);
        }

        this.timeoutInstance = setTimeout(
            () => {
                this.updateQuery(this.queryString);
            },
            this.queryTimeout,
        );
    }

    protected onQueryInput({target}: {target: HTMLInputElement} | any) {
        if (!target) {
            return;
        }
        this.queryString = target.value;
    }

    protected selectTag(tag) {
        if (!this.isMultiple) {
            this.updateValue(tag);
        }   else {
            const val: any = [...this.computedValue];

            if (val.indexOf(tag) === -1) {
                val.push(tag);
            }   else {
                val.splice(val.indexOf(tag), 1);
            }

            this.updateValue(val);
        }

    }

    @Emit('query')
    protected updateQuery(query: string) {
        return query;
    }

    protected toggleOptionsContainerVisibility() {
        this.isOptionsContainerVisible = !this.isOptionsContainerVisible;
    }

    protected setOptionsContainerVisibility(value: boolean) {
        this.isOptionsContainerVisible = value;
    }

    @Watch('isMultiple')
    protected async handleMultipleValue(value: boolean) {
        await waitForTicks(this);
        if (value) {
            this.updateValue(this.value ? [this.value] : null);
        }   else {
            this.updateValue(this.value ? this.value[0] : null);
        }
    }
}

export default SelectFormElementMixin;
