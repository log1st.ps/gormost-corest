import { availableFormInputTypes } from '../../../../../constants/form/input/formInputTypesConstants';
import {ICommonFormElement} from '../CommonFormElementMixin.d';

interface IInputFormElement extends ICommonFormElement {
    type?: availableFormInputTypes;
    min?: number;
    max?: number;
    step?: number;
    rows?: number;
}
