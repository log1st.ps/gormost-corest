import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import {
    availableFormInputTypes,
    availableFormInputTypesList,
    INPUT_TYPE_TEXT,
} from '../../../../../constants/form/input/formInputTypesConstants';
import APIProp, { PROP_TYPE_NUMBER, PROP_TYPE_STRING } from '../../../../../helpers/documentation/APIProp';
import CommonFormElementMixin from '../CommonFormElementMixin';
import {IFieldType} from '../CommonFormElementMixin.d';
import { IInputFormElement } from './InputFormElementMixin.d';

@Component
class Mixin extends VueComponent<IInputFormElement> {}

export const FIELD_TYPE_INPUT: IFieldType = 'input';

// tslint:disable-next-line:max-classes-per-file
@Component
class InputFormElementMixin
    extends Mixins(Mixin, CommonFormElementMixin)
    implements IInputFormElement {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Type of input',
        availableValues: availableFormInputTypesList,
        defaultValue: INPUT_TYPE_TEXT,
    })
    @Prop({ default: () => INPUT_TYPE_TEXT })
    public type ?: availableFormInputTypes;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Min value if input type is number',
    })
    @Prop()
    public min ?: number;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Max value if input type is number',
    })
    @Prop()
    public max ?: number;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Step if input type is number',
    })
    @Prop({ default: () => 1 })
    public step ?: number;

    @APIProp({
        type: PROP_TYPE_NUMBER,
        description: 'Rows count if input type is textarea',
    })
    @Prop({ default: () => 1 })
    public rows ?: number;
}

export default InputFormElementMixin;
