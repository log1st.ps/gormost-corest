import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import InputFormElementMixin from './InputFormElementMixin';

describe('InputFormElementMixin', () => {
    [
        'type',
        'min',
        'max',
        'step',
        'rows',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(InputFormElementMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
