import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import CommonFormElementMixin from './CommonFormElementMixin';

describe('CommonFormElementMixin', () => {
    [
        'placeholder',
        'value',
        'isDisabled',
        'isReadonly',
        'isValid',
        'isInvalid',
        'isClearable',
        'clearValue',
        'hasAppearance',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(CommonFormElementMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
