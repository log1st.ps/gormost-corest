import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import { readFileAsBase64 } from '../../../../../helpers/fileHelper';
import waitForTicks from '../../../../../helpers/waitForTicks';
import FileFormElementMixin from './FileFormElementMixin';

describe('FileFormElementMixin', () => {
    [
        'isMultiple',
        'acceptableFiles',
        'isDropZone',
        'isAppendable',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(FileFormElementMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it('updates computedFiles on value change', () => {
        [
            'test',
            'test2',
            'test3',
        ].forEach((value: string) => {
            const wrapper = shallowMount(FileFormElementMixin, {
                propsData: {
                    value,
                },
            });

            const check = [
                {
                    url: value,
                    name: '#1',
                    type: null,
                },
            ];

            // @ts-ignore
            expect(wrapper.vm.computedFiles).to.deep.equal(check);
        });
    });

    it('emits value on handleFiles', async () => {
        const wrapper = shallowMount(FileFormElementMixin);

        const fileName = 'test';
        const fileType = 'text/html';

        const testFile = new File([''], fileName, { type: fileType });

        // @ts-ignore
        wrapper.vm.handleFiles([testFile]);

        await waitForTicks(wrapper.vm, 3);

        expect(wrapper.emitted().input[0][0]).to.be.deep.equal({
            name: fileName,
            type: fileType,
            url: await readFileAsBase64(testFile),
        });
    });
});
