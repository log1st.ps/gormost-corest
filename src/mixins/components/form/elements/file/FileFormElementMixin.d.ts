import {ICommonFormElement} from '../CommonFormElementMixin.d';

export interface IFileFormElement extends ICommonFormElement {
    isMultiple?: boolean;
    acceptableFiles?: string[];
    isDropZone?: boolean;
    isAppendable?: boolean;
}
