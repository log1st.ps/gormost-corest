import { Component, Mixins, Prop, Watch } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../../../helpers/documentation/APIProp';
import { downloadFileByType, readFileAsBase64 } from '../../../../../helpers/fileHelper';
import waitForTicks from '../../../../../helpers/waitForTicks';
import CommonFormElementMixin from '../CommonFormElementMixin';
import {IFieldType} from '../CommonFormElementMixin.d';
import { IFileFormElement } from './FileFormElementMixin.d';

export interface IComputedFile {
    url: FileReader['result'];
    name: string | null;
    type: string | null;
}

@Component
class Mixin extends VueComponent<IFileFormElement> {
}

export const FIELD_TYPE_FILE: IFieldType = 'file';

// tslint:disable-next-line:max-classes-per-file
@APIComponent({
    slots: [
        {
            name: 'file',
            description: 'Content of file',
            params: {
                url: [PROP_TYPE_STRING, PROP_TYPE_OBJECT],
                name: PROP_TYPE_STRING,
                type: PROP_TYPE_STRING,
            },
        },
    ],
})
@Component
class FileFormElementMixin
    extends Mixins(Mixin, CommonFormElementMixin)
    implements IFileFormElement {

    // @ts-ignore
    public $refs: {
        input: HTMLInputElement,
    };

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        defaultValue: false,
    })
    @Prop()
    public isMultiple ?: boolean;

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Array of MIME-types or file extensions of acceptable files',
        defaultValue: null,
    })
    @Prop()
    public acceptableFiles ?: string[];

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if field can accept file drops',
        defaultValue: false,
    })
    @Prop()
    public isDropZone ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Set if field should append files if isDropzone equals to true',
        defaultValue: false,
    })
    @Prop()
    public isAppendable ?: boolean;

    protected computedFiles: IComputedFile[] = [];

    @Watch('value', {
        immediate: true,
    })
    public setComputedFiles() {
        let values: any[] = [];

        if (this.isMultiple) {
            if (Array.isArray(this.value)) {
                values = values.concat(this.value);
            } else {
                values.push(this.value);
            }
        } else if (Array.isArray(this.value)) {
            if (this.value.length > 0) {
                values = values.concat(this.value);
            }
        } else {
            values.push(this.value);
        }

        const files: IComputedFile[] = [];

        values
            .filter(Boolean)
            .forEach((file: any, index: number) => {
                const computedFile: IComputedFile = {
                    url: null,
                    name: null,
                    type: null,
                };

                if (typeof file === 'object') {
                    ['name', 'url', 'type'].forEach((key: string) => {
                        if (key in file) {
                            computedFile[key] = file[key];
                        }
                    });
                } else {
                    computedFile.name = `#${index + 1}`;
                    computedFile.url = file;
                }

                files.push(computedFile);
            });

        this.computedFiles = files;
    }

    private async handleFiles(sourceFiles: File[]) {
        const files = this.isMultiple
            ? sourceFiles
            : sourceFiles.length > 0 ? [sourceFiles[0]] : [];

        const value: IComputedFile[] = [];

        await Promise.all(
            files.map(async (file) => {
                value.push({
                    url: file instanceof File ? await readFileAsBase64(file) : file,
                    name: file.name,
                    type: file.type,
                });
            }),
        );

        this.updateValue(
            this.isMultiple
                ? (
                    this.isAppendable
                        ? [...this.computedFiles, ...value]
                        : [...value]
                )
                : (value[0] || null),
        );
    }

    private showFileSelector() {
        if (this.isDisabled || this.isReadonly) {
            return;
        }

        const input: HTMLInputElement = document.createElement('input');
        input.type = 'file';
        input.multiple = !!this.isMultiple;

        input.addEventListener(
            'change', ({ target: { files } }: any) => {
                this.handleFiles(Array.from(files as FileList));
            },
        );

        input.click();
    }

    private downloadFile(index: number) {
        if (this.isDisabled) {
            return;
        }

        const { url, name } = this.computedFiles[index];

        if (url) {
            downloadFileByType(url, name);
        }
    }

    private removeFile(index: number) {
        if (!this.isMultiple) {
            this.updateValue(null);
        }   else {
            const newFiles = [...this.computedFiles];
            this.updateValue(newFiles.slice(index, 1));
        }
    }

    private onDrop(e: DragEvent) {
        e.preventDefault();

        if (!e.dataTransfer) {
            return;
        }

        this.handleFiles(Array.from(e.dataTransfer.files));
    }

    @Watch('isDropZone', {
        immediate: true,
    })
    private async handleDnDEvents() {
        await waitForTicks(this, 20);

        if (!this.$refs.input) {
            return;
        }

        if (this.isDropZone) {
            this.$refs.input.addEventListener('drop', this.onDrop);
        }   else {
            this.$refs.input.removeEventListener('drop', this.onDrop);
        }
    }
}

export default FileFormElementMixin;
