import {availableIconTypes} from '../../../../constants/icon/iconTypesConstants';

export interface ICommonFormElement {
    placeholder?: string;
    value?: any;
    isDisabled?: boolean;
    isReadonly?: boolean;
    isValid?: boolean;
    isInvalid?: boolean;
    isClearable?: boolean;
    clearValue?: any;
    hasAppearance?: boolean;
    before?: string | availableIconTypes;
    after?: string | availableIconTypes;
    texts?: {
        [key: string]: string,
    };
}

export type IFieldType =
    'checkbox' | 'datepicker' | 'field' | 'file' | 'input' | 'select';
