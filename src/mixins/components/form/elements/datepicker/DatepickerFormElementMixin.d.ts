import {ICommonFormElement} from '../CommonFormElementMixin.d';

export interface IDatepickerFormElement extends ICommonFormElement {
    format?: string;
    portalName?: string;
}
