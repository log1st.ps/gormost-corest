import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import DatepickerFormElementMixin from './DatepickerFormElementMixin';

describe('DatepickerFormElementMixin', () => {
    [
        'format',
        'portalName',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(DatepickerFormElementMixin);

            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it('has prop format default value equal to "dd.mm.YYYY"', () => {
        const wrapper = shallowMount(DatepickerFormElementMixin);

        expect(wrapper.vm.format).to.be.equal('dd.mm.YYYY');
    });

    it('has prop portalName default value equal to "datepicker"', () => {
        const wrapper = shallowMount(DatepickerFormElementMixin);

        expect(wrapper.vm.portalName).to.be.equal('datepicker');
    });
});
