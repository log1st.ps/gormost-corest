import { Component, Mixins, Prop } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import APIComponent from '../../../../../helpers/documentation/APIComponent';
import APIProp, { PROP_TYPE_STRING } from '../../../../../helpers/documentation/APIProp';
import CommonFormElementMixin from '../CommonFormElementMixin';
import {ICommonFormElement, IFieldType} from '../CommonFormElementMixin.d';
import { IDatepickerFormElement } from './DatepickerFormElementMixin.d';
import {PORTAL_DATEPICKER} from '../../../../../constants/portal/portalNames';

@Component
class Mixin extends VueComponent<IDatepickerFormElement> {}

export const FIELD_TYPE_DATEPICKER: IFieldType = 'datepicker';

// tslint:disable-next-line:max-classes-per-file
@APIComponent({

})
@Component
class DatepickerFormElementMixin
    extends Mixins(Mixin, CommonFormElementMixin)
    implements IDatepickerFormElement, ICommonFormElement {

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Input value format',
        defaultValue: 'dd.mm.YYYY',
    })
    @Prop({ default: () => 'dd.mm.YYYY' })
    public format ?: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Portal name',
        defaultValue: () => 'datepicker',
    })
    @Prop({ default: () => PORTAL_DATEPICKER })
    public portalName ?: string;
}

export default DatepickerFormElementMixin;
