import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import FormComponentMixin from './FormComponentMixin';

describe('FormComponentMixin', () => {
    [
        'id',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(FormComponentMixin);
            expect(wrapper.props()).to.contain.keys(prop);
        });
    });
});
