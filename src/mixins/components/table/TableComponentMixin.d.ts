export interface ITableColumn {
    key: string;
    title?: string;
    subTitle?: string;
}
export interface ITableData {
    value?: string;
    hint?: string;
}

export interface ITableDataRow {
    [key: string]: ITableData | string | number | undefined;
}

export interface ITable {
    keyField: string;
    data: ITableDataRow[];
    columns: ITableColumn[];
    visibleKeys?: string[];
    caption?: string;
    columnsOrder?: string[];
    filteredColumnsKeys?: string[];
    isBordered?: boolean;
    isFixed?: boolean;
    isVerticallyStriped?: boolean;
    isHorizontallyStriped?: boolean;
    hasFooter ?: boolean;
    isCondensed ?: boolean;
}
