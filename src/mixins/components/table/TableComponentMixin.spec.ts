import { shallowMount } from '@vue/test-utils';
import { expect } from 'chai';
import TableElementMixin from './TableComponentMixin';

describe('ButtonElementMixin', () => {
    [
        'columns',
        'data',
        'caption',
        'columnsOrder',
        'visibleKeys',
        'filteredColumnsKeys',
        'keyField',
        'isBordered',
        'isVerticallyStriped',
        'isHorizontallyStriped',
    ].forEach((prop: string) => {
        it(`contains props.${prop}`, () => {
            const wrapper = shallowMount(TableElementMixin);
            expect(wrapper.props()).to.contain.keys(prop);
        });
    });

    it('check sorted columns', () => {
        const wrapper = shallowMount(TableElementMixin);
        wrapper.setProps({
            keyField: '+',
            visibleKeys: ['b', 'a', 't', 'l', 'g'],
            columnsOrder: ['b', 'a', 't'],
            columns: [
                { key: 't', value: 1 },
                { key: 'l', value: 2 },
                { key: 'a', value: 3 },
                { key: 'b', value: 4 },
                { key: 'g', value: 5 },
            ],
        });
        expect(
            wrapper.vm.sortedColumns.map(el => el.key),
        ).to.eql(['b', 'a', 't', 'l', 'g']);
    });
});
