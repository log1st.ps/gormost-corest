import {Component, Mixins, Prop} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import APIComponent from '../../../helpers/documentation/APIComponent';
import APIProp, {
    PROP_TYPE_ARRAY,
    PROP_TYPE_BOOLEAN,
    PROP_TYPE_NUMBER,
    PROP_TYPE_OBJECT,
    PROP_TYPE_STRING,
} from '../../../helpers/documentation/APIProp';
import EmptyRender from '../../../mixins/EmptyRender';
import {ITable, ITableColumn, ITableDataRow} from './TableComponentMixin.d';

@Component
class Mixin extends VueComponent<ITable> {}

@APIComponent({
    slots: [
        {
            name: 'default',
            description: 'If there is no data',
        },
        {
            name: 'header',
            description: 'Instead of thead',
        },
        {
            // tslint:disable-next-line:no-invalid-template-strings
            name: 'h_${column.key}',
            description: 'Instead of exact th',
            params: {
                '...columns[n]': {
                    type: PROP_TYPE_OBJECT,
                    description: 'Properties from column',
                },
            },
        },
        {
            name: 'row',
            description: 'Instead of each row\'s each column',
            params: {
                row: {
                    type: PROP_TYPE_OBJECT,
                    description: 'Exact record',
                },
                index: PROP_TYPE_NUMBER,
            },
        },
        {
            // tslint:disable-next-line:no-invalid-template-strings
            name: 'r_${column.key}',
            description: 'Instead of each row',
            params: {
                column: {
                    type: PROP_TYPE_OBJECT,
                    description: 'Column object',
                },
                row: {
                    type: PROP_TYPE_OBJECT,
                    description: 'Exact record',
                },
                index: PROP_TYPE_NUMBER,
            },
        },
        {
            name: 'caption',
            description: 'Instead of caption tag for summary etc.',
        },
    ],
})

// tslint:disable-next-line:max-classes-per-file
@Component
class TableComponentMixin
extends Mixins(Mixin, EmptyRender)
    implements ITable {
    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Data which will be placed in the <th> tag',
        contains: {
            key: {
                type: PROP_TYPE_STRING,
                description: 'field name. ',
                isRequired: true,
            },
            title: {
                type: PROP_TYPE_STRING,
                description: 'column title, located in the headers',
            },
            subTitle: {
                type: PROP_TYPE_STRING,
                description: 'column subtitle, located in the headers',
            },
        },
    })
    @Prop()
    public columns !: ITableColumn[];

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Data which will be placed in the <td> tag',
        contains: {
            value: {
                type: PROP_TYPE_STRING,
                description: 'field value',
                isRequired: false,
            },
            hint: {
                type: PROP_TYPE_STRING,
                description: 'field hint',
                isRequired: false,
            },
        },
    })
    @Prop()
    public data !: ITableDataRow[];

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Text to be placed inside the <caption> tag',
    })
    @Prop()
    public caption ?: string;

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Indicates the sequence of columns',
    })
    @Prop({ default: () => [] })
    public columnsOrder ?: string[];

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Indicates which table columns will be visible',
    })
    @Prop({ })
    public visibleKeys ?: string[];

    @APIProp({
        type: PROP_TYPE_ARRAY,
        description: 'Highlight the specified table columns',
    })
    @Prop()
    public filteredColumnsKeys ?: string[];

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Minimizing rendering https://vuejs.org/v2/api/#key',
    })
    @Prop()
    public keyField !: string;

    @APIProp({
        type: PROP_TYPE_STRING,
        description: 'Will it be indicated Table layout Fixed in css',
    })
    @Prop()
    public isFixed ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Will the frame in the table, the default is false',
    })
    @Prop({ default: () => false })
    public isBordered ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Striped table columns',
    })
    @Prop()
    public isVerticallyStriped ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Striped table rows',
    })
    @Prop()
    public isHorizontallyStriped ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Does table has footer',
    })
    @Prop()
    public hasFooter ?: boolean;

    @APIProp({
        type: PROP_TYPE_BOOLEAN,
        description: 'Is condensed',
    })
    @Prop()
    public isCondensed ?: boolean;

    get sortedColumns(): ITableColumn[] {
        const order = this.columnsOrder || [];
        const count = order.length - 1;
        const visible = this.visibleKeys || [];
        const columns = visible.length
            ? this.columns.filter(({ key }) => visible.indexOf(key) > -1)
            : this.columns;
        return order.length
            ? columns.sort(({ key: a }, { key: b }) => {
                const x = order.indexOf(a);
                const y = order.indexOf(b);
                return (x === -1 ? count : x) - (y === -1 ? count : y);
            })
            : columns;
    }
}

export default TableComponentMixin;
