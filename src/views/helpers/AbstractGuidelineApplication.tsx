import {Component, Watch} from 'vue-property-decorator';
import {VueComponent} from 'vue-tsx-helper';
import waitForTicks from '../../helpers/waitForTicks';
import {getItem, removeItem, setItem} from '../../helpers/storageHelper';
import {PortalTarget} from 'portal-vue';

interface INumberKnobOptions {
    min?: number;
    max?: number;
    step?: number;
}

interface ITextareaKnobOptions {
    rows?: number;
}

interface ICheckboxKnobOptions {
    trueValue: any;
    falseValue: any;
}

interface IListKnobOptions {
    options: Array<string | { key: string, text: string }>;
    isMultiple?: boolean;
    allowEmpty?: boolean;
}

type IKnobOptions = {} | INumberKnobOptions | ITextareaKnobOptions | ICheckboxKnobOptions | IListKnobOptions;

type IComponentParamType = 'input' | 'number' | 'textarea' | 'checkbox' | 'list' | 'object';

export type IComponentParam = {
    type: IComponentParamType,
    value: any,
    label?: string,
    options?: IKnobOptions,
} | string;

export interface IStoryComponent {
    component: typeof VueComponent;
    model?: {
        prop: string;
        event: string;
    };
    params?: {
        [key: string]: IComponentParam,
    };
    defaultSlot?: string;
    events?: string[];
    handlers?: {
        [key: string]: (component: IStoryComponent, ...args) => void,
    };
}

interface IKnob {
    key: string;
    label?: string;
    type: string;
    model: string;
    options: IKnobOptions;

    onChange(value: any): void;
}

export interface IGuidelineStyles {
    page: string;
    container: string;
    select: string;
    wrapper: string;
    preview: string;
    clearKnobs: string;
    previewWithTransparentBackground: string;
    controls: string;
    tabs: string;
    tab: string;
    active: string;
    actionsList: string;
    action: string;
    actionName: string;
    knobs: string;
    knob: string;
    knobName: string;
    knobControl: string;
    knobCheckbox: string;
    topControls: string;
    knobLabel: string;
    pane: string;
    panes: string;
    actionArguments: string;
}

@Component({})
class AbstractGuidelineApplication extends VueComponent<{}> {

    // Implementation below.

    get computedComponent(): IStoryComponent {
        return this.components[this.activeComponent];
    }

    get computedEvents(): string[] {
        return this.computedComponent.events || [];
    }

    get computedParams(): IStoryComponent['params'] {
        return this.computedComponent.params;
    }

    get computedAttrs(): { [key: string]: any } {
        return Object.entries(this.computedParams || {}).reduce(
            (currentValue, [key, param]: [string, IComponentParam]) => ({
                ...currentValue,
                [key]: (typeof param === 'string' ? param : param.value),
            }),
            {},
        );
    }

    get computedKnobs(): IKnob[] {
        return Object.entries(this.computedParams || {}).map(
            ([key, param]: [string, IComponentParam]): IKnob => ({
                key,
                type: typeof param === 'object' ? param.type : 'input',
                model: typeof param === 'object' ? param.value : param,
                label: typeof param === 'object' ? param.label : undefined,
                onChange: ({target}: Event) => {
                    let {
                        value,
                    } = target as HTMLInputElement;
                    if (!target) {
                        return;
                    }

                    if (typeof param === 'string') {
                        if (this.computedParams) {
                            this.computedParams[key] = value;
                        }
                    }   else if (param.type === 'number') {
                        param.value = +value;
                    } else if (typeof param === 'object') {
                        if (
                            param.type === 'checkbox'
                            && param.options
                            && 'trueValue' in param.options
                        ) {
                            value = {
                                [param.options.trueValue]: param.options.falseValue,
                                [param.options.falseValue]: param.options.trueValue,
                            }[value];
                        }

                        param.value = value;
                    }
                },
                options: typeof param === 'string' ? {} : (param.options || {}),
            }),
        );
    }

    protected portals: string[] = [];

    // @ts-ignore
    protected styles: IGuidelineStyles = {};

    protected components: {
        [key: string]: IStoryComponent,
    } = {};

    protected activeComponent !: string;

    protected availableTabs: string[] = [
        'knobs',
        'actions',
    ];

    protected currentTab: string = 'knobs';

    protected emittedActions: Array<{
        name: string,
        args: {
            [key: string]: any,
        },
    }> = [];

    protected initialConfig: {
        [key: string]: IStoryComponent,
    } = {};

    protected isBackgroundTransparent = true;

    public handleDefaultSlotValue(event: {target: HTMLInputElement} | any) {
        this.computedComponent.defaultSlot = event.target.value;
    }

    public render() {
        const {styles} = this;
        return (
            <div class={styles.page}>
                <div class={styles.container}>
                    <h1>Integrated components list:</h1>
                    <div class={styles.topControls}>
                        <select
                            class={styles.select}
                            v-model={this.activeComponent}
                        >
                            {Object.entries(this.components).map(([key]) => (
                                <option value={key}>{key}</option>
                            ))}
                        </select>
                        <label class={styles.knobName}>
                            <input
                                type={'checkbox'}
                                class={[
                                    styles.knobControl,
                                    styles.knobCheckbox,
                                ]}
                                // @ts-ignore
                                value={this.isBackgroundTransparent}
                                checked={this.isBackgroundTransparent}
                                onInput={this.toggleTransparentBackground}
                            />
                            <span class={styles.knobLabel}>"Transparent" background</span>
                        </label>
                    </div>
                    <div class={styles.wrapper}>
                        <div class={{
                            [styles.preview]: true,
                            [styles.previewWithTransparentBackground]: this.isBackgroundTransparent,
                        }}>
                            <this.computedComponent.component
                                {...{
                                    attrs: this.computedAttrs,
                                    on: {
                                        ...(
                                            this.computedComponent.model
                                                ? {
                                                    [this.computedComponent.model.event]: (value) => {
                                                        this.updateModel(value);
                                                    },
                                                }
                                                : {}
                                        ),
                                        ...(
                                            this.computedEvents.reduce<{}>(
                                                (currentValue, item) => ({
                                                    ...currentValue,
                                                    [item]: (...args) => {
                                                        if (this.computedComponent.model) {
                                                            if (item === 'input') {
                                                                this.updateModel(args[0]);
                                                            }
                                                        }
                                                        if (this.computedComponent.handlers) {
                                                            if (item in this.computedComponent.handlers) {
                                                                this.computedComponent.handlers[item](
                                                                    this.computedComponent,
                                                                    ...args,
                                                                );
                                                            }
                                                        }
                                                        this.emitEvent(item, args);
                                                    },
                                                }),
                                                {},
                                            )
                                        ),
                                    },
                                }}
                            >
                                {this.computedComponent.defaultSlot && (
                                    <div>
                                        {this.computedComponent.defaultSlot}
                                    </div>
                                )}
                            </this.computedComponent.component>
                        </div>
                        <div class={styles.controls}>
                            <div
                                class={styles.clearKnobs}
                                onClick={this.clearModel}
                            >...Clear model
                            </div>
                            <div class={styles.tabs}>
                                {this.availableTabs.map((tab) => (
                                    <button
                                        class={{
                                            [styles.tab]: true,
                                            [styles.active]: tab === this.currentTab,
                                        }}
                                        onClick={() => this.setTab(tab)}
                                    >
                                        {
                                            tab === 'actions'
                                                ? `actions (${this.emittedActions.length})`
                                                : `knobs (${this.computedKnobs.length})`
                                        }
                                    </button>
                                ))}
                            </div>
                            <div class={styles.panes}>
                                {this.currentTab === 'knobs' && (
                                    <div class={styles.pane}>
                                        <div class={styles.knobs}>
                                            {'defaultSlot' in this.computedComponent && (
                                                <div class={styles.knob}>
                                                    <div class={styles.knobName}>Default slot children</div>
                                                    <input
                                                        class={styles.knobControl}
                                                        value={this.computedComponent.defaultSlot}
                                                        onInput={this.handleDefaultSlotValue}
                                                    />
                                                </div>
                                            )}
                                            {this.computedKnobs.map(
                                                ({key, label, type, model, onChange, options}) => (
                                                    <div class={styles.knob}>
                                                        <div class={styles.knobName}>{label || key}</div>
                                                        {
                                                            ({
                                                                input: () => (
                                                                    <input
                                                                        class={styles.knobControl}
                                                                        value={model}
                                                                        onInput={onChange}
                                                                    />
                                                                ),
                                                                number: (opts: INumberKnobOptions) => (
                                                                    <input
                                                                        class={styles.knobControl}
                                                                        value={model}
                                                                        onInput={onChange}
                                                                        min={opts.min}
                                                                        max={opts.max}
                                                                        step={opts.step}
                                                                    />
                                                                ),
                                                                textarea: (opts: ITextareaKnobOptions) => (
                                                                    <textarea
                                                                        class={styles.knobControl}
                                                                        onInput={onChange}
                                                                        rows={opts.rows}
                                                                    >{model}</textarea>
                                                                ),
                                                                checkbox: (opts: ICheckboxKnobOptions) => (
                                                                    <input
                                                                        type={'checkbox'}
                                                                        class={[
                                                                            styles.knobControl,
                                                                            styles.knobCheckbox,
                                                                        ]}
                                                                        onInput={onChange}
                                                                        checked={model === (opts.trueValue || true)}
                                                                        value={model}
                                                                    />
                                                                ),
                                                                object: () => (
                                                                    <div>{typeof model}</div>
                                                                ),
                                                                list: (opts: IListKnobOptions) => (
                                                                    <select
                                                                        class={styles.knobControl}
                                                                        onChange={onChange}
                                                                    >
                                                                        {[
                                                                            ...(
                                                                                opts.allowEmpty
                                                                                    ? [{key: '', text: '...choose'}]
                                                                                    : []
                                                                            ),
                                                                            ...opts.options,
                                                                        ].map((option) => (
                                                                            <option
                                                                                key={
                                                                                    typeof option === 'string'
                                                                                        ? option
                                                                                        : option.key
                                                                                }
                                                                                value={
                                                                                    typeof option === 'string'
                                                                                        ? option
                                                                                        : option.key
                                                                                }
                                                                                selected={
                                                                                    (
                                                                                        typeof option === 'string'
                                                                                            ? option
                                                                                            : option.key
                                                                                    ) === model
                                                                                }
                                                                            >
                                                                                {
                                                                                    typeof option === 'string'
                                                                                        ? option
                                                                                        : option.text
                                                                                }
                                                                            </option>
                                                                        ))}
                                                                    </select>
                                                                ),
                                                            }[type])(options)
                                                        }
                                                    </div>
                                                ))}
                                        </div>
                                    </div>
                                )}
                                {this.currentTab === 'actions' && (
                                    <div class={styles.pane}>
                                        <div class={styles.actionsList}>
                                            {this.emittedActions.map((action) => (
                                                <div class={styles.action}>
                                                    <div class={styles.actionName}>
                                                        {action.name}
                                                    </div>
                                                    <div class={styles.actionArguments}>
                                                        {JSON.stringify(action.args)}
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                {this.portals.map((portal) => (
                    // @ts-ignore
                    <PortalTarget name={portal}/>
                ))}
            </div>
        );
    }

    @Watch('activeComponent')
    public async handleComponentUpdate() {
        await waitForTicks(this);

        this.clearModel();

        await waitForTicks(this);

        this.emittedActions = [];
    }

    public created(): void {
        this.initialConfig = JSON.parse(JSON.stringify(this.components));
        this.setInitials();
    }

    protected updateModel(value: any) {
        if (
            !this.computedComponent.model
            || !this.computedParams
        ) {
            return;
        }


        const param = this.computedParams[this.computedComponent.model.prop];

        if (typeof param === 'object') {
            param.value = value;
        }   else {
            this.computedParams[this.computedComponent.model.prop] = value;
        }
    }

    protected toggleTransparentBackground() {
        this.isBackgroundTransparent = !this.isBackgroundTransparent;
    }

    protected setInitials() {
        const component = getItem('guideline:component');
        const tab = getItem('guideline:tab');
        const model = getItem('guideline:model');

        this.isBackgroundTransparent = getItem('guideline:isBackgroundTransparent');

        if (component) {
            this.activeComponent = component;
        }
        if (tab) {
            this.currentTab = tab;
        }
        if (
            model
            && this.components
            && this.activeComponent
            && this.activeComponent in this.components
        ) {
            Object.entries(model).forEach(([key, value]) => {
                const params = this.components[this.activeComponent].params;

                if (!params || !(key in params)) {
                    return;
                }

                if (typeof params[key] === 'object') {
                    // @ts-ignore
                    params[key].value = value;
                }
                if (typeof params[key] === 'string') {
                    // @ts-ignore
                    params[key] = value;
                }
            });
        }
    }

    protected clearModel() {
        removeItem('guideline:model');

        const clearSource: {[key: string]: IStoryComponent} = JSON.parse(JSON.stringify(this.initialConfig));

        Object.entries(clearSource).forEach(([key, {params}]) => {
            this.components[key].params = params;
        });
    }

    protected emitEvent(name: string, args: { [key: string]: any }) {
        this.emittedActions.push({
            name,
            args,
        });
    }

    protected setTab(tab: string) {
        this.currentTab = tab;
    }

    @Watch('activeComponent')
    @Watch('currentTab')
    @Watch('isBackgroundTransparent')
    @Watch('computedKnobs', {
        deep: true,
    })
    protected async saveStateToStore() {
        await waitForTicks(this);

        setItem('guideline:component', this.activeComponent);
        setItem('guideline:isBackgroundTransparent', this.isBackgroundTransparent);
        setItem('guideline:tab', this.currentTab);
        setItem('guideline:model', this.computedKnobs.reduce(
            (currentValue, {key, model}) => ({
                ...currentValue,
                [key]: model,
            }),
            {},
        ));
    }
}

export {AbstractGuidelineApplication};
