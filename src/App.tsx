import { Component } from 'vue-property-decorator';
import { VueComponent } from 'vue-tsx-helper';
import './assets/root.scss';

@Component
class App extends VueComponent<{

}> {
    public render() {
        return (
            <div>
                text
            </div>
        );
    }
}

export default App;
