import findScrollableElement from './findScrollableElement';
import {
    availablePositionObserverAligns,
    availablePositionObserverPositions,
} from '../constants/helpers/positionObserverConstants';

export interface IPositionObserverParams {
    target: HTMLElement;
    popup: HTMLElement;
    position?: availablePositionObserverPositions;
    align?: availablePositionObserverAligns;
    primaryOffset?: number;
    secondaryOffset?: number;
    isSticky?: boolean;
    onArrowPositionUpdate(availablePositionObserverPositions: string): void;
}

export interface IPositionObserver {
    observe(): IPositionObserver;
    destroy(): IPositionObserver;
}

export default function({
    target,
    popup,
}: IPositionObserverParams): IPositionObserver {
    let timeout;

    const handleEvent = () => {
        const {
            top: targetTop,
            left: targetLeft,
        } = target.getBoundingClientRect();

        const targetHeight = target.offsetHeight;
        const targetWidth = target.offsetWidth;

        const top = targetTop + targetHeight;
        const left = targetLeft;
        const width = targetWidth;

        window.requestAnimationFrame(() => {
            popup.style.top = `${top}px`;
            popup.style.left = `${left}px`;
            popup.style.width = `${width}px`;
        });
    };

    const scroller = findScrollableElement(target);

    return {
        observe(): IPositionObserver {
            scroller.addEventListener('scroll', handleEvent);
            window.addEventListener('resize', handleEvent);
            timeout = setInterval(handleEvent, 1000);

            handleEvent();

            return this;
        },
        destroy(): IPositionObserver {
            scroller.removeEventListener('scroll', handleEvent);
            window.removeEventListener('resize', handleEvent);
            clearInterval(timeout);

            return this;
        },
    };
}
