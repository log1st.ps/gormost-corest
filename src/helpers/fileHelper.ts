export const readFileAsBase64 = (
    file: File,
) => new Promise<FileReader['result']>((resolve, reject) => {
    if (process.env.NODE_ENV === 'test') {
        resolve(process.env.FILE_STUB || 'some file');
    }   else {
        const reader = new FileReader();

        reader.addEventListener('load', () => {
            resolve(reader.result);
        });

        reader.addEventListener('error', reject);

        reader.readAsDataURL(file);
    }
});

export const downloadFileByType = (
    file: FileReader['result'],
    name: string | null,
    type: 'url' = 'url',
) => {
    let url: typeof file | null = null;
    if (type === 'url') {
        url = file;
    } else if (type === 'file') {
        url = file;
    }

    if (!url) {
        return;
    }

    const a: HTMLAnchorElement = document.createElement('a');
    a.download = name || 'download';
    a.href = url instanceof ArrayBuffer
        ? String.fromCharCode.apply(null, new Uint16Array(url) as unknown as number[])
        : url;

    a.click();
};
