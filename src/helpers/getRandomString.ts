export default (length: number = 7) => Math.random().toString(36).substring(length);
