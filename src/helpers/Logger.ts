/* tslint:disable-next-line:no-unused-expression */

type availableLogTypes =
    'log' | 'error' | 'warn';

export const LOG_CATEGORY_COMPONENT_METHOD = 'componentMethod';
export const LOG_CATEGORY_OTHER = 'other';

type availableLogCategories =
    typeof LOG_CATEGORY_COMPONENT_METHOD
    | typeof LOG_CATEGORY_OTHER;

const baseLog = (type: availableLogTypes, category: availableLogCategories, text: any) => {
    if (['production', 'test'].indexOf(process.env.NODE_ENV) > -1) {
        return false;
    }
    console[type](
        `[${type}]`,
        ...(Array.isArray(text) ? text : [text]),
    );
};

export const errorLog = (type: availableLogCategories = LOG_CATEGORY_OTHER, ...texts: any) => {
    baseLog('error', type, texts);
};

export const warnLog = (type: availableLogCategories = LOG_CATEGORY_OTHER, ...texts: any) => {
    baseLog('warn', type, texts);
};

export const log = (type: availableLogCategories = LOG_CATEGORY_OTHER, ...texts: any) => {
    baseLog('log', type, texts);
};
