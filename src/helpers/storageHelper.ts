export const setItem = (key: string, value: any) => {
    localStorage.setItem(key, JSON.stringify(value));
};

export const getItem = (key: string) => {
    const value = localStorage.getItem(key);
    if (!value) {
        return null;
    }
    return JSON.parse(value);
};

export const removeItem = (key: string) => {
    localStorage.removeItem(key);
};

