import { VNode } from 'vue';

export const isSlotExists = function(name: string) {
    // @ts-ignore
    return name in { ...this.$scopedSlots, ...this.$slots };
};

export const renderSlot = function(
    name: string,
    fallback ?: ((props: any) => void) | VNode,
    props: any = {},
) {
    // @ts-ignore
    return this._t(
        name,
        fallback && (typeof fallback === 'function' ? fallback(props) : fallback),
        props,
    );
};
