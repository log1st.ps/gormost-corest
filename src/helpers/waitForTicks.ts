import { Vue } from 'vue-property-decorator';

export default async (vm: Vue, count: number = 1) => {
    await Promise.all(
        Array(count).fill(null).map(
            async () => {
                await vm.$nextTick;
            },
        ),
    );
};
