import { VueComponent } from 'vue-tsx-helper';

interface IAPIModel {
    type: string;
    event: string;
    description?: string;
    defaultValue?: any;
}

export default ({
    type,
    event,
    description,
}: IAPIModel) => (
    (target: VueComponent<any>, propertyKey: string): void => {
    }
);
