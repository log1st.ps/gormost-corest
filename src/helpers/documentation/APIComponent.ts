import { availablePropTypes, IAPIProp } from './APIProp';

interface IComponentSlot {
    name: string;
    params?: string[] | {
        [key: string]: IAPIProp | availablePropTypes | availablePropTypes[],
    };
    description?: string;
}

export default ({
    slots,
}: {
    slots?: IComponentSlot[],
}) => (
    <T extends new(...args: any[]) => {}>(constructor: T) => {
        return class extends constructor {

        };
    }
);
