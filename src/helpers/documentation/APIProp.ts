import { VueComponent } from 'vue-tsx-helper';

export const PROP_TYPE_STRING = 'String';
export const PROP_TYPE_NUMBER = 'Number';
export const PROP_TYPE_BOOLEAN = 'Boolean';
export const PROP_TYPE_ARRAY = 'Array';
export const PROP_TYPE_OBJECT = 'Object';
export const PROP_TYPE_DATE = 'Date';
export const PROP_TYPE_FUNCTION = 'Function';
export const PROP_TYPE_SYMBOL = 'Symbol';
export const PROP_TYPE_ANY = 'Any';

export type availablePropTypes =
    typeof PROP_TYPE_STRING |
    typeof PROP_TYPE_NUMBER |
    typeof PROP_TYPE_BOOLEAN |
    typeof PROP_TYPE_ARRAY |
    typeof PROP_TYPE_OBJECT |
    typeof PROP_TYPE_DATE |
    typeof PROP_TYPE_FUNCTION |
    typeof PROP_TYPE_SYMBOL |
    typeof PROP_TYPE_ANY;

export interface IAPIProp  {
    type?: availablePropTypes | availablePropTypes[];
    description?: string;
    defaultValue?: any;
    availableValues?: any;
    isRequired?: boolean;
    isSugar?: boolean;
    contains?: {
        [key: string]: IAPIProp | availablePropTypes | availablePropTypes[],
    };
}

export default ({
    type,
    description,
    defaultValue,
    availableValues,
    contains,
    isRequired = false,
    isSugar = false,
}: IAPIProp) => (
    (target: VueComponent<any> | any, propertyKey: string): void => {
    }
);
