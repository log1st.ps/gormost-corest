import { VueComponent } from 'vue-tsx-helper';
import { availablePropTypes } from './APIProp';

export const EVENT_ARGUMENT_TYPE_OTHER = 'other';

export default ({
 name,
 args,
    description,
}: {
    name: string,
    args?: Array<{
        name: string,
        type?: availablePropTypes | typeof EVENT_ARGUMENT_TYPE_OTHER,
        other?: string,
    }>,
    description?: string,
}) => (
    (target: VueComponent<any>, propertyKey: string): void => {

    }
);
