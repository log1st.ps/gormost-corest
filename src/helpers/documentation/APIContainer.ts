import { availablePropTypes, IAPIProp } from './APIProp';

export interface IContainerSlot {
    name: string;
    params?: string[] | {
        [key: string]: IAPIProp | availablePropTypes | availablePropTypes[],
    };
    description?: string;
}

export default ({
    slots,
    description,
}: {
    slots?: IContainerSlot[],
    description?: string;
} = {}) => (
    <T extends new(...args: any[]) => {}>(constructor: T) => {
        return class extends constructor {

        };
    }
);
