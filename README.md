Install dependencies

```yarn install```

Serve

```yarn serve```

Tests

```yarn test:unit```